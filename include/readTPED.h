#ifndef  readtped_INC
#define  readtped_INC



extern "C"                 
{
// Get declaration for intel MKL
#include "mkl.h"           
#include "mkl_vml.h"       
#include "mkl_vsl.h"       
#include "mkl_vsl_functions.h"
#include "mkl_vml_functions.h"
#include "mkl_blas.h"
#include "mkl_lapack.h"
}

#include <cstdlib>
#include <string>
#include <algorithm>
#include <iterator>
#include <vector>
#include <boost/unordered_map.hpp>

#include <fstream>
#include <iostream>

#include <boost/lexical_cast.hpp>
#include <boost/tokenizer.hpp>

using namespace boost;
using namespace std;

class readTPED 
{
public:

    readTPED( string prefix, string phenFilename, string covariateFilename );
    ~readTPED();

    double* getSNPMatrix() const { return m_snpMatrix; } 
    double* getPhenMatrix() const { return m_phenMatrix; } 
    double* getCovMatrix() const { return m_covMatrix; }
    

    void computeKinship( bool isIBS );
    void readKinship( string kinshipFile );
    double* getKinship() const { return m_kinshipMatrix; }


    int getNumInds() const { return m_numInds; }
    long getNumSNPs() const { return m_numSNPs; }
    int getNumPhens() const { return m_numPhens; }
    int getNumCovs() const { return m_numCovs; }

    bool getSNPid( string rsID, unsigned int &SNPID );
    string getSNPrsid( unsigned int snpid ) { return m_rsID[ snpid ]; }
    long getSNPpos( unsigned int snpid )    { return m_posSNPs[ snpid ]; }
    double getSNPmaf( unsigned int snpid )  { return m_mafSNPs[ snpid ]; }
    
    double getSNPvar( unsigned int snpid )  { return m_varSNPs[ snpid ]; }
    void   setSNPvar( unsigned int snpid, double var ) { m_varSNPs[ snpid ] = var; }
    double getSNPchr( unsigned int snpid )  { return m_chrSNPs[ snpid ]; }

private:
    int m_numInds; 
    int m_numPhens;
    int m_numSNPs;
    int m_numCovs;

    double *m_snpMatrix, *m_phenMatrix;
    double *m_varSNPs, *m_mafSNPs;
    double *m_varPhens, *m_meanPhens;
    
    long *m_posSNPs;
    int *m_chrSNPs;
    
    vector< string > m_rsID;
    boost::unordered_map< string, unsigned int > m_rsIdToId;

    double *m_kinshipMatrix;
    double *m_covMatrix;
};







#endif   /* ----- #ifndef readtped_INC  ----- */
