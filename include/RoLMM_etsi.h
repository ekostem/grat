

#ifndef  rolmm_INC
#define  rolmm_INC


extern "C"                 
{
// Get declaration for intel MKL
#include "mkl.h"           
#include "mkl_vml.h"       
#include "mkl_vsl.h"       
#include "mkl_vsl_functions.h"
#include "mkl_vml_functions.h"
#include "mkl_blas.h"
#include "mkl_lapack.h"
}

#include "readTPED.h"
#include "methodBisection.h"


#include <string>
#include <iostream>
#include <fstream>

#include <cmath>
#include <boost/math/distributions/fisher_f.hpp>


using namespace boost;
using namespace std;

class readTPED;
class bisectionSolver;

class RoLMM
{
public:
    RoLMM( int numInds, int numCovs, int numPhens, string outPrefix );
    ~RoLMM();

    void setKinship( double *K, double evThreshold );
    double const *getU() { return m_U; }
    
    void setSNPMatrix( double *snpMatrix, int numSNPs );
    void setX( double *X, bool isLM );
    void setY( double *Y, bool isLM );


    void findMax(bool isREML, bool isPlot);
    void testSNPs( bool isREML );
    
    void setPfilter( double pfilter) { m_pfilter = pfilter; }
    void setReader( readTPED *reader ) { m_reader = reader; }

    void testSNPsLM( bisectionSolver *rules );
    void testSNPsLMM( bisectionSolver *rules, bool isREML );

private:
    /* helper functions */ 
    double trace(double *mat, int n) { double sum=0.0; for(int i=0; i<n; i++){ sum +=mat[i*n+i]; } return sum; }
    void transpose( double *source, double *target, int nrow, int ncol ); // transpose source to target
    void scaleColumns( double *mat, double *vec, int nrow, int ncol ); // X %*% D, D is diagonal
    void scaleRows( double *mat, double *vec, int nrow, int ncol); // D %*% X, D is diagonal
    void getEigenDecomposition(double *X, int N, double *eVecs, double *eVals);
    void getProjection( double* P, double *X, int nrow, int ncol );
    /*****************/

    void setYLMM( int phenNo );
    void update();

    /****gets sets*********/
    void setDelta( double delta ){ m_delta = delta; update(); }
    double getDelta( ) { return m_delta; }
    double getHeritability(double varE );
    double getLL() { return m_LL; }
    double getdLL() { return m_dLL; }
    double getREMLL() { return m_REMLL; }
    double getdREMLL() { return m_dREMLL; }
    /**********************/


    boost::math::fisher_f *m_dist;
    readTPED *m_reader;
    

    double m_pfilter;
    string m_infoFilename, m_pvalueFilename;
    
    int m_up, m_low; // stores dim of eigenvector
    int m_numCovs;
    int m_numInds;
    int m_numSNPs;
    int m_numPhens;

    double m_delta;
    double m_sigmaG, m_sigmaE;
    double m_sigmaGreml, m_sigmaEreml;
    double m_LL, m_dLL, m_d2LL;
    double m_REMLL, m_dREMLL, m_d2REMLL;

    double *m_K;
    double *m_S, *m_U;

    double *m_XtX, *m_invXtX, m_detXtX;
   
    double *m_XltXl;
    double *m_XltYl;

    double *m_P;

    double *m_X, *m_Y;

    double *m_Xor, *m_Yor; // original data
    double *m_Xu, *m_Xl; // splitted by low rank
    double *m_Yu, *m_Yl;


    double *m_beta,   *m_dbeta;
    double *m_Xubeta, *m_Xudbeta;
    double *m_Xlbeta, *m_Xldbeta;

    double *m_snpMatrix;

};


#endif   /* ----- #ifndef rolmm_INC  ----- */
