/*
 * =====================================================================================
 *
 *       Filename:  mathHelper.h
 *
 *    Description:  Some normal dist. functions like the cdf approx.
 *
 *        Version:  1.0
 *        Created:  10/19/2011 01:24:15
 *       Revision:  none
 *       Compiler:  icpc
 *
 *         Author:  EMRAH KOSTEM (ekostem), 
 *        Company:  www.emrahkostem.com
 *
 * =====================================================================================
 */




#ifndef  mathHelper_INC
#define  mathHelper_INC




#include <string>
#include <algorithm>

using namespace std;

extern "C"
{
// Get declaration for intel MKL
#include "math.h"
#include "mathimf.h"
#include "mkl.h"
#include "mkl_vml.h"
#include "mkl_vsl.h"
#include "mkl_vsl_functions.h"
#include "mkl_vml_functions.h"
#include "mkl_blas.h"
#include "mkl_lapack.h"
}



const double PI = 3.14159265358979;
const double SQRT2PI = 2.506628274631;
const double M_SQRT2PI = 0.398942280401433;

string itoa(int value, int base);


/* 
 * 1D Normal Distribution functions
 * Are vectorized....
 */

int pnormVec(const double* x, double* output, const int n);
int dnormVec(const double* x, double* output, const int n);
int qnormVec(const double* x, const double* mean, const double* var, double* output, const int n);


/* 
 * 2D Normal cdf to compute expected recall 
 * by sampling
 */


class RandomVariable
{
public:
    RandomVariable(const int seed, const int brng=VSL_BRNG_MT19937);
    ~RandomVariable() {vslDeleteStream( &stream_);}
    void generate_normal(const int number_variables, const int mean, const int std, double* output, const int generate_method=VSL_METHOD_DGAUSSIAN_ICDF);
    void mvgenerate_normal(const int number_variables, int dimension, const double* mean, double* variance_covariance_matrix, double* output, const int generate_method=VSL_METHOD_DGAUSSIANMV_ICDF);
    void generate_uniform(const int number_variables, const double left, const double right, double* output, const int generate_method=VSL_METHOD_DUNIFORM_STD);
                                                
                                                            
private:
    VSLStreamStatePtr stream_;
    int brng_;
    int seed_;
};



double computeQuadrantProb(RandomVariable *rv,double s, double t, double mean1, double mean2, double rho, int sampleSize);
double computeQuadrantProb(double s, double t, double mean1, double mean2, double rho);

inline double nc(double x)
{
    double y;
    vdCdfNorm(1, &x, &y);
    return y;
    //double result;
    //if (x<-7.)
    //        result = ndf(x)/sqrt(1.+x*x);
    //        else if (x>7.)
    //                result = 1. - nc(-x);
    //                else
    //                {
    //                result = 0.2316419;
    //                static double a[5] = {0.31938153,-0.356563782,1.781477937,-1.821255978,1.330274429};
    //                result=1./(1+result*fabs(x));
    //                result=1-ndf(x)*(result*(a[0]+result*(a[1]+result*(a[2]+result*(a[3]+result*a[4])))));
    //                if (x<=0.) result=1.-result;
    //                }
    //                return result;
    //                }
}



inline double max(double a, double b)
{
    if (a>b) return a;
    return b;
}


inline double min(double a, double b)
{
    if (a>b) return b;
    return a;
}


inline double sgn(double x)
{
    if ( x >= 0.0 ) return 1.0;
    return -1.0;
}

inline double fxy(double x, double y, double a, double b, double rho)
{
    double a_s;
    double b_s;
    double result;
    a_s = a / sqrt(2 * (1 - rho * rho));
    b_s = b / sqrt(2 * (1 - rho * rho));
    result = exp(a_s * (2 * x - a_s) + b_s * (2 * y - b_s) + 2 * rho * (x - a_s) * (y - b_s));
    return result;
}


inline double Ntwo(double a, double b, double rho)
{
    static double aij[4]={0.325303,0.4211071,0.1334425,0.006374323};
    static double bij[4]={0.1337764,0.6243247,1.3425378,2.2626645};
    double result(0.0);;
    for(int i=0;i<=3;i++) 
    {
        for(int j=0;j<=3;j++)
        {
            result+=aij[i] * aij[j] * fxy(bij[i], bij[j], a, b, rho); 
        }
    }
    result = result * sqrt(1 - rho * rho) / PI;
    return result;
}

inline double ND2(double a, double b, double rho)
{
    double rho1;
    double rho2;
    double denominator;
    double result;

    if (rho > 0.9999)
        result = nc(min(a, b));
    else if (rho < -0.9999)
        result = max(0, nc(a) - nc(-b));
    else
        {
        if (a * b * rho <= 0) 
                {
            if (a <= 0 && b <= 0 && rho <= 0)
                result = Ntwo(a, b, rho);
            else if (a <= 0 && b * rho >= 0)
                result = nc(a) - Ntwo(a, -b, -rho);
            else if (b <= 0 && rho >= 0)
                result = nc(b) - Ntwo(-a, b, -rho);
            else
                result = nc(a) + nc(b) - 1 + Ntwo(-a, -b, rho);
                }
        else
                {
            denominator = sqrt(a * a - 2 * rho * a * b + b * b);
            rho1 = (rho * a - b) * sgn(a) / denominator;
            rho2 = (rho * b - a) * sgn(b) / denominator;
            result = ND2(a, 0, rho1) + ND2(b, 0, rho2) - (1 - sgn(a) * sgn(b)) / 4;
        }
        if (result < 0) result = 0;
        }
        return result;
}




#endif   /* ----- #ifndef mathHelper_INC  ----- */
