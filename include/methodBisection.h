/*
 * =====================================================================================
 *
 *       Filename:  methodBisection.h
 *
 *    Description:  Contains all the functions I need
 *
 *        Version:  1.0
 *        Created:  10/19/2011 09:46:10
 *       Revision:  none
 *       Compiler:  icpc
 *
 *         Author:  EMRAH KOSTEM (ekostem), 
 *        Company:  www.emrahkostem.com
 *
 * =====================================================================================
 */


#ifndef  methodbisection_INC
#define  methodbisection_INC




extern "C"
{
// Get declaration for intel MKL
#include "mkl.h"
#include "mkl_vml.h"
#include "mkl_vsl.h"
#include "mkl_vsl_functions.h"
#include "mkl_vml_functions.h"
#include "mkl_blas.h"
#include "mkl_lapack.h"
}

#include <ctime>

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <list>
#include <stack>
#include <set>
#include <algorithm>
#include <boost/unordered_map.hpp>

#include <boost/lexical_cast.hpp>
#include <boost/tokenizer.hpp>


#include "mathHelper.h"
#include "readTPED.h"
#include "RoLMM_etsi.h"

using namespace boost;
using namespace std;


/* 
 * All method functions have the same argument list
 */

struct decisionRule
{
    unsigned int candidate;
    unsigned int tag;
    double s;
    double r;
    double candidatePR;
    double tagPR;
    double expectedCost;
    double expectedRecall;
    bool operator==(const decisionRule &rhs) const
    {
        return ( candidate == rhs.candidate ) && ( tag == rhs.tag );
    }
};


struct SNPinfo
{
    unsigned int id;
    string chrom;
    long bp;
    double maf;
};

struct SNPcmpBP 
{
    bool operator()( const SNPinfo &l, const SNPinfo &r ) const
    {
        return l.bp < r.bp;
    }
};


struct snpLD
{
    unsigned int index;
    double r;
    int *isTag;

    snpLD( unsigned int a=0, double b=0.0, int *p=NULL ): index(a), r(b), isTag(p) {} 
    bool operator < (const snpLD &rhs ) const
    {
        return fabs(r) > fabs(rhs.r);
    }

    bool operator == (const snpLD &rhs ) const
    {
        return index == rhs.index;
    }
};

struct sortLD
{
    bool operator()(const snpLD& lhs, const snpLD& rhs)
    {
        return fabs(lhs.r) > fabs(rhs.r);
    }
};


struct testRule
{
    testRule( unsigned int id, double stat ): remainderId( id ), statThreshold( stat ) { }
    unsigned int remainderId;
    double statThreshold;
    //bool operator < (const testRule &other) const { return statThreshold < other.statThreshold; }
};

struct testRuleCmpStat
{
    bool operator() ( const testRule &l, const testRule &r ) const
    {
        return l.statThreshold < r.statThreshold;
    }
};




class readTPED;
class RoLMM;

class bisectionSolver
{

public:
    
    typedef vector< testRule > proxySNPRuleSet;
    typedef multiset < snpLD > setBestTag;
    typedef vector< pair< unsigned int, unsigned int > > proxyNo_ID;
    typedef double (bisectionSolver::*MemFn)(double , double, double );
    typedef double (bisectionSolver::*MemFnVec)( double , list< decisionRule >* , const double*);

    bisectionSolver( readTPED *reader, RoLMM *lmm, bool isLM, string proxyFilename, string ruleFilename, double ALPHA, double POWER, double SENSITIVITY, double RMIN, int MAXDISTANCE, double minMAF );
    ~bisectionSolver();
    
    int run(bool expedite );
    int runBatch( bool expedite );
    void test();
    void save();

    vector< proxySNPRuleSet > const * getProxyRules() { return &m_proxyRules; }
    proxyNo_ID const * getProxyNo_ID() { return &m_proxyNo_ID; }

private:
    
    boost::unordered_map< unsigned int,  list< unsigned int >* > m_proxyPairs;
    
    vector< proxySNPRuleSet > m_proxyRules;
    proxyNo_ID m_proxyNo_ID;

    int m_numInds;
    unsigned int m_numSNPs;
    
    
    int m_MAXDISTANCE;
    double m_RMIN;
    double m_ALPHA, m_POWER, m_NCP, m_SENSITIVITY, m_THRHLD;
    double m_TOL, m_NMAX;
    double m_MINMAF;

    vector< vector<double> > m_tableEC, m_tableER;
    vector< double > m_tableS;
    int m_rGrid, m_sGrid;

    bool m_fixedProxies, m_isProxyStart;
    double m_minExpectedCost, m_expectedRecallRate;
        
    vector< setBestTag > *m_corrMatrix;
    vector< int > m_snpLabel;
    
    double *m_cs; // vector of causal probability priors.
    vector<double> *m_costProgression;
    vector<double> *m_recallRateProgression;
    vector<double> *m_gradientProgression;
    list<decisionRule> *m_solution;
    list<unsigned int> *m_candidateSNPs;
    list<unsigned int> *m_tagSNPs;
    
    double powerFunction(double ncp); 
    double getThreshold(double alpha);
    void updateLookUp(double gradient);

    double expectedCost(double s, double c, double r );
    double expectedRecall(double s, double c, double r);
    double expectedCostDerivative(double s, double c, double r);
    double expectedRecallDerivative(double s, double c, double r);
    double totalExpectedCost(const list< decisionRule > *pairs);
    double totalExpectedRecall(const list< decisionRule > *pairs);
    double bisection( MemFn f, double c, double r, double targetValue, const double* interval, double tol); 
    double bisectionVec( MemFnVec f, list< decisionRule > *pairs, double targetValue, const double* interval, double tol); 
    double mapLogGradient2S(double logGradient, double c,double r, const double* interval);
    double mapLogGradient2TotalER(double logGradient, list< decisionRule > *pairs, const double* interval);
    double mapLogGradient2TotalEC(double logGradient, list< decisionRule > *pairs, const double * interval);
    void mapLogGradient2TotalPerf(double logGradient, list< decisionRule > *pairs, double &totalEC, double &totalER);
    void mapLogGradient2TotalPerfLookUp(double logGradient, list< decisionRule > *pairs, double &totalEC, double &totalER);

    double mapS2LogGradient(double s, double c, double r);

    int solverX( list<decisionRule> *pairs, double targetER, double &ec, double &er, double gr);
    int solver( list< decisionRule > *pairs, double targetER, double &ec, double &er, double &gr );
   
    double getCorr( unsigned int, unsigned int );

    inline double lookUpEC( double s, double r){ return m_tableEC[ floor( m_sGrid*s) ][ floor( (r+1.0)*m_rGrid ) ];}
    inline double lookUpER( double s, double r){ return m_tableER[ floor( m_sGrid*s) ][ floor( (r+1.0)*m_rGrid ) ];}
    inline double lookUpS( double r){ return m_tableS[ floor( (r+1.0)*m_rGrid ) ];} 

    void getBestTagPairs(const list<unsigned int> *candidates, list<decisionRule> *pairs); 
    void updateTags( const vector< unsigned int> *newTags, list< unsigned int > *candList, list< unsigned int > *tagList );
    void undoTags( const vector< unsigned int> *removeTags, list< unsigned int > *candList, list< unsigned int > *tagList );
    void fixTags( vector< unsigned int > *newTags,  list< unsigned int > *candList, list< unsigned int > *tagList );
    void printBestTagTable( list< unsigned int> *candidates ); 
    
    double diffclock(clock_t clock1,clock_t clock2);
    
};


#endif   /* ----- #ifndef methodbisection_INC  ----- */
