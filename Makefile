OS = $(shell uname -s)
CXX = icpc -std=c++0x  
CFLAGS = -O3 -DNDEBUG -Wall -vec-report1

LIBDIR = -L$(MKLROOT)/lib/intel64 
INCLUDE = -I$(MKLROOT)/include

LIBDIR += -L$(BOOST_INTEL)/lib
INCLUDE += -I$(BOOST_INTEL)/include

ifeq ($(OS),Darwin)
	#CFLAGS += -gcc-name=/opt/local/bin/gcc-mp-4.7  -mmacosx-version-min=10.8 -static-intel
	CFLAGS += -static-intel -gcc-name=/usr/bin/gcc  -mmacosx-version-min=10.8 
	#LIBS = -lmkl_solver_lp64 -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -liomp5 -lpthread
	LIBS = -lboost_program_options
	LIBS += -lmkl_intel_lp64 -lmkl_sequential -lmkl_core -lpthread
endif

ifeq ($(OS),Linux)
	CFLAGS += -static -static-intel
	#LIBS = -lmkl_solver_lp64 -Wl,--start-group -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -Wl,--end-group -liomp5 -lpthread
	LIBS = -lboost_program_options
	#LIBS += -lmkl_solver_lp64_sequential -Wl,--start-group -lmkl_intel_lp64 -lmkl_sequential -lmkl_core -Wl,--end-group -lpthread
	LIBS += -Wl,--start-group -lmkl_intel_lp64 -lmkl_sequential -lmkl_core -Wl,--end-group -lpthread
endif

OBJECTS = readTPED.o mathHelper.o methodBisection.o RoLMM_etsi.o main.o

all : $(OBJECTS)
	$(CXX) $(CFLAGS) $(OBJECTS) $(INCLUDE) $(LIBDIR) $(LIBS) -o grat
	ctags -R --sort=yes --c++-kinds=+p --fields=+iaS --extra=+q .


main.o : ./src/main.cpp
	$(CXX) $(CFLAGS) -c ./src/main.cpp $(INCLUDE) $(LIBDIR) $(LIBS)


mathHelper.o : ./src/mathHelper.cpp	
	$(CXX) $(CFLAGS) -c ./src/mathHelper.cpp $(INCLUDE) $(LIBDIR) $(LIBS)


methodBisection.o : ./src/methodBisection.cpp
	$(CXX) $(CFLAGS) -c ./src/methodBisection.cpp $(INCLUDE) $(LIBDIR) $(LIBS)


RoLMM_etsi.o : ./src/RoLMM_etsi.cpp
	$(CXX) $(CFLAGS) -c ./src/RoLMM_etsi.cpp $(INCLUDE) $(LIBDIR) $(LIBS)


readTPED.o : ./src/readTPED.cpp
	$(CXX) $(CFLAGS) -c ./src/readTPED.cpp $(INCLUDE) $(LIBDIR) $(LIBS)

clean:
	@echo $(OS)
	rm *.o
