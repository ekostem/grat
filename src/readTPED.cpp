#include "../include/readTPED.h"

readTPED::readTPED( string prefix, string phenoFile, string covariateFilename )
{
    m_snpMatrix = NULL;
    m_phenMatrix = NULL;
    m_covMatrix = NULL;
    
    m_varSNPs = NULL;
    m_mafSNPs = NULL;
    m_posSNPs = NULL;
    m_chrSNPs = NULL;

    m_meanPhens = NULL;
    m_varPhens = NULL;

    m_kinshipMatrix = NULL;

    m_numInds = 0;
    m_numSNPs = 0;
    m_numPhens = 0;
    m_numCovs = 0;


    if ( !prefix.size() || !phenoFile.size() || !covariateFilename.size() )
    {
        cerr << "Missing file" << endl;
        abort();
    }


    boost::char_separator<char> sep(" \t");
    typedef boost::tokenizer< boost::char_separator<char> > Tokenizer;


    string tfam( prefix + ".tfam" );
    ifstream fp( tfam.c_str() );
    if ( !fp.is_open() )
    {
        cerr << "Error reading .TFAM file" << endl;
        abort();
    }
    else
    {
        vector< string > vec;
        string line;
        while( getline( fp, line) )
        {
            m_numInds++;
            Tokenizer tok(line, sep);
            vec.assign( tok.begin(), tok.end() );
        }
        cout << "#numInds: " << m_numInds << endl;
    }
    fp.close();

    
    string tped( prefix + ".tped" );
    fp.open( tped.c_str() );
    if ( !fp.is_open() )
    {
        cerr << "Error reading .TPED file" << endl;
        abort();
    }
    else
    {
        m_numSNPs = count(istreambuf_iterator<char>(fp), istreambuf_iterator<char>(), '\n');
        double d_one = 1.0;
        double d_zero = 0.0;
        int incx = 1;
        int NM = m_numInds*m_numSNPs;
        m_snpMatrix = new double[ NM ];
        dscal( &NM, &d_zero, m_snpMatrix, &incx );

        string line;
        fp.clear();
        fp.seekg(0, ios::beg );

        vector<string> vec;
        double *ones = new double[ m_numInds ];
        for (int i=0; i<m_numInds; i++) ones[i] = 1.0;
        m_varSNPs = new double[ m_numSNPs ];
        m_mafSNPs = new double[ m_numSNPs ];
        m_posSNPs = new long[ m_numSNPs ];
        m_chrSNPs = new int[ m_numSNPs ];
        unsigned int id = 0;
        double N = (double)m_numInds;
        while( getline(fp, line ) )
        {
            Tokenizer tok( line, sep );
            vec.assign( tok.begin(), tok.end() );
            replace( vec.begin()+4, vec.end(), "0", "1" );            
            for ( int i=0; i<m_numInds; i++ )
            {
                m_snpMatrix[ id*m_numInds + i ] = strtod( vec[ 4+2*i].c_str(), NULL) + strtod( vec[ 5+2*i].c_str(), NULL ) - 2.0;
            }
            double maf( -dasum(&m_numInds, &m_snpMatrix[id*m_numInds], &incx) / (double)m_numInds );

            m_rsID.push_back( vec[1] );
            m_rsIdToId[ vec[1] ] = id;
            m_mafSNPs[ id ] = (-0.5*maf > 0.5) ? (1.0 + 0.5*maf) : -0.5*maf;
            m_varSNPs[ id ] = ddot( &m_numInds, &m_snpMatrix[id*m_numInds], &incx, &m_snpMatrix[id*m_numInds], &incx )/(N-1.0) - N/(N-1.0)*pow(maf,2);

            /*
            if ( !isIBS )
            {
                daxpy( &m_numInds, &maf, ones, &incx, &m_snpMatrix[id*m_numInds], &incx ); // subtract mean
                //double sd( 1.0/sqrt( 2.0* m_mafSNPs[ id ] *( 1.0 - m_mafSNPs[ id ] )  )  );
                double sd( 1.0/sqrt( m_varSNPs[id] ) );
                dscal( &m_numInds, &sd,  &m_snpMatrix[id*m_numInds], &incx ); 
            }
            */

            m_posSNPs[ id ] = atol( vec[3].c_str() );
            m_chrSNPs[ id ] = atoi( vec[0].c_str() );
            id++;
        }
        cout << "#numSNPs: " << m_numSNPs << endl;
        delete [] ones;
    }//else
    fp.close();

    fp.open(phenoFile.c_str());
    if ( !fp.is_open() )
    {
        cerr << "Error reading Phenotype file" << endl;
        abort();
    }
    else
    {
        vector< string > vec;
        string line;
        getline(fp, line);
        Tokenizer tok(line,sep);
        vec.assign( tok.begin(), tok.end() );
        m_numPhens = vec.size() - 2; // subtract 2 because famID, indID 
        m_phenMatrix = new double[ m_numPhens*m_numInds ];
        m_varPhens = new double[ m_numPhens ];
        m_meanPhens = new double[ m_numPhens ];        
        
        fp.clear();
        fp.seekg(0, ios::beg);
        unsigned int id = 0;
        while ( getline( fp, line) )
        {
            Tokenizer tok(line,sep);
            vec.assign( tok.begin(), tok.end() );
            for (int i=0; i < m_numPhens; i++)
            {
                m_phenMatrix[ i*m_numInds + id] = strtod( vec[2+i].c_str(), NULL );
            }
            id++;
        }
        cout << "#numPhens: " << m_numPhens << endl;
    }
    fp.close();


    fp.open(covariateFilename.c_str());
    if ( !fp.is_open() )
    {
        cerr << "Error reading Covariate file" << endl;
    }
    else
    {
        vector< string > vec;
        string line;
        getline(fp, line);
        Tokenizer tok(line,sep);
        vec.assign( tok.begin(), tok.end() );
        m_numCovs = vec.size() - 2; // subtract 2 because famID, indID 
        m_covMatrix = new double[ m_numCovs*m_numInds ];
        
        fp.clear();
        fp.seekg(0, ios::beg);
        unsigned int id = 0;
        while ( getline( fp, line) )
        {
            Tokenizer tok(line,sep);
            vec.assign( tok.begin(), tok.end() );
            for (int i=0; i<m_numCovs; i++)
            {
                m_covMatrix[ i*m_numInds + id] = strtod( vec[2+i].c_str(), NULL );
            }
            id++;
        }
        cout << "#numCovariates: " << m_numCovs << endl;
    }
    fp.close();

}


readTPED::~readTPED()
{

    if ( m_snpMatrix ) delete [] m_snpMatrix;
    if ( m_phenMatrix ) delete [] m_phenMatrix;
    if ( m_covMatrix ) delete [] m_covMatrix;
    
    if ( m_varSNPs ) delete [] m_varSNPs;
    if ( m_mafSNPs ) delete [] m_mafSNPs;
    if ( m_posSNPs ) delete [] m_posSNPs;
    if ( m_chrSNPs ) delete [] m_chrSNPs;

    if ( m_varPhens ) delete [] m_varPhens;
    if ( m_meanPhens ) delete [] m_meanPhens;

    if ( m_kinshipMatrix ) delete [] m_kinshipMatrix;
}

void readTPED::computeKinship(bool isIBS)
{
    if ( !m_kinshipMatrix )
    {
        m_kinshipMatrix = new double[ m_numInds * m_numInds ];
    }

    double scale = 1.0 / (double)m_numSNPs;
    double beta = 0.0;
    dgemm( "N", "T", &m_numInds, &m_numInds, &m_numSNPs, &scale, m_snpMatrix, &m_numInds, m_snpMatrix, &m_numInds, &beta, m_kinshipMatrix, &m_numInds);
    ofstream fp;
    fp.open("kinship.txt" );
    for ( int i=0; i<m_numInds; i++ )
    {
        fp << m_kinshipMatrix[ i*m_numInds ];
        for ( int j=1; j<m_numInds; j++ )
        {
            fp << " " << m_kinshipMatrix[ i*m_numInds + j ];
        }
        fp << endl;
    }
    fp.close();
}

void readTPED::readKinship( string kinshipFile )
{
    
    if ( !m_kinshipMatrix )
    {
        m_kinshipMatrix = new double[ m_numInds*m_numInds ];
    }
    
    ifstream fp;
    boost::char_separator<char> sep(" \t");
    typedef boost::tokenizer< boost::char_separator<char> > Tokenizer;

    fp.open(kinshipFile.c_str());
    if ( !fp.is_open() )
    {
        cerr << "Error reading kinship file" << endl;
    }
    else
    {
        vector< string > vec;
        string line;
        unsigned int id = 0;
        while ( getline( fp, line) )
        {
            Tokenizer tok(line,sep);
            vec.assign( tok.begin(), tok.end() );
            for (int i=0; i<m_numInds; i++)
            {
                m_kinshipMatrix[ i*m_numInds + id] = strtod( vec[i].c_str(), NULL );
            }
            id++;
        }
        for ( int i=0; i < m_numInds; i++ )
        {
            m_kinshipMatrix[ i*m_numInds + i ] = 1.0;
        }
    }
    fp.close();
}

bool readTPED::getSNPid( string rsID, unsigned int &SNPID ) 
{ 
    boost::unordered_map<string, unsigned int>::const_iterator got = m_rsIdToId.find(rsID);
    if ( got == m_rsIdToId.end() )
    {
        return false;
    }
    else
    {
        SNPID = got->second;
        return true;
    }
}

