/*
 * =====================================================================================
 *
 *       Filename:  methodBisection.cpp
 *
 *    Description:  Implementation of the method
 *
 *        Version:  1.0
 *        Created:  10/19/2011 11:53:15
 *       Revision:  none
 *       Compiler:  icpc
 *
 *         Author:  EMRAH KOSTEM (ekostem), 
 *        Company:  www.emrahkostem.com
 *
 * =====================================================================================
 */

#include "../include/methodBisection.h"

bisectionSolver::bisectionSolver(readTPED *reader, RoLMM *lmm, bool isLM, string proxyFilename, string ruleFilename, double ALPHA, double POWER, double SENSITIVITY, double RMIN, int MAXDISTANCE, double minMAF )
{
    if ( !reader )
    {
        cerr << "Error accessing TPED resource" << endl;
        abort();
    }

    m_corrMatrix = NULL;
    m_cs = NULL;
    m_costProgression = NULL;
    m_recallRateProgression = NULL;
    m_gradientProgression = NULL;
    m_solution = NULL;
    m_minExpectedCost = 0;
    m_expectedRecallRate = 0;
     
    m_fixedProxies = true;
    m_isProxyStart = false;

    m_RMIN = RMIN;
    m_MAXDISTANCE = MAXDISTANCE;
    m_MINMAF = minMAF;

    m_ALPHA = ALPHA;
    m_POWER = POWER;
    m_THRHLD = getThreshold(m_ALPHA);
    m_SENSITIVITY = SENSITIVITY;

    m_NMAX = 100;
    m_TOL = 1e-15;
    
    double left = 0.0;
    double right = 7.0;
    double ncp = 5.0;

    while ( fabs(powerFunction(ncp) - m_POWER) > m_TOL )
    {
        if ( powerFunction(ncp) > m_POWER )
        {
            right = ncp;
            ncp = 0.5*( left + right );
        }
        else 
        {
            left = ncp;
            ncp = 0.5*( left + right );
        }
    }

    m_NCP = ncp;   
    cout << "NCP: " << m_NCP << endl;
    cout << "Power: " << m_POWER << endl;
    cout << "Alpha: " << m_ALPHA << endl;
    cout << "minMAF: " << m_MINMAF << endl;

    char_separator<char> sep(" \t");
    typedef boost::tokenizer< boost::char_separator<char> > Tokenizer;


    // readALL SNPs from TPED reader and assign as a candidate SNP
    m_numSNPs = reader->getNumSNPs();
    m_numInds = reader->getNumInds();
    m_candidateSNPs = new list< unsigned int >;
    m_tagSNPs = new list< unsigned int >;
    m_cs = new double[ m_numSNPs ];
    for ( unsigned int i=0; i<m_numSNPs; i++ )
    {
        //m_candidateSNPs->push_back( i );
        m_snpLabel.push_back( 0 );
        m_cs[ i ] = 1e-5;
    }

    /*
    * Read Fixed proxies
    */
    ifstream fp;
    if ( proxyFilename.size() )
    {
        fp.open( proxyFilename.c_str() );
        if ( !fp.is_open() )
        {
            cerr << "Error reading PROXY file" << endl;

        }
        else
        {
            vector< string > vec;
            string line;
            unsigned int snpID;
            while( getline(fp, line) )
            {
                Tokenizer tok( line, sep);
                vec.assign( tok.begin(), tok.end() );
                if ( reader->getSNPid( vec[0] , snpID ) )
                {
                    if ( reader->getSNPmaf( snpID ) > m_MINMAF && reader->getSNPvar( snpID ) )
                    {
                        m_tagSNPs->push_back( snpID );
                        m_snpLabel[ snpID ] = 1;
                    }
                }
            }
            m_tagSNPs->sort();
            m_tagSNPs->unique();
            m_fixedProxies = true; 
        }
        cout << "#Proxies: " << m_tagSNPs->size() << endl;
        fp.close(); 
    }
    /*
    * Read proxy-remainder pairs
    */
    /*
     * READ pairs and store the remainders of a proxy in a lookup table
     */

    list< unsigned int >::iterator it;
    for ( it = m_tagSNPs->begin(); it != m_tagSNPs->end(); it++ )
    {
        m_proxyPairs[ *it ] = new list< unsigned int >;
    }


    if ( ruleFilename.size() )
    {
        fp.open( ruleFilename.c_str() );
        if ( !fp.is_open() )
        {
            cerr << "Error reading RULE file" << endl;
        }
        else
        {
            vector< string > vec;
            string line;
            unsigned int remainderID, proxyID;
            while( getline(fp, line) )
            {
                Tokenizer tok( line, sep);
                vec.assign( tok.begin(), tok.end() );
                if ( reader->getSNPid( vec[0] , remainderID )  && reader->getSNPid( vec[1], proxyID)  )
                {
                    if ( reader->getSNPvar( remainderID )  && reader->getSNPmaf( remainderID ) > m_MINMAF )
                    {
                        // If the proxy is kosher
                        if ( reader->getSNPvar( proxyID ) && reader->getSNPmaf( proxyID ) > m_MINMAF )
                        {
                            m_candidateSNPs->push_back( remainderID );
                            m_proxyPairs[ proxyID ]->push_back( remainderID );
                        }
                        else
                        {
                            // This candidate is alone make it a proxy
                            m_tagSNPs->push_back( remainderID );
                            m_snpLabel[ remainderID ] = 1;
                        }
                    }
                }
            }
        }
        fp.close(); 
    }

    /*
     * CREATE THE CORRELATION MATRIX FOR THE REMAINDER SNPS IN SNPLIST
     * USING MB DISTANCE THRESHOLD
     */
    m_corrMatrix = new vector< setBestTag >;
    m_corrMatrix->resize( m_numSNPs );
    
    double *snpMatrix = reader->getSNPMatrix();    
    int incx = 1;
    
    int N2 = m_numInds*m_numInds;
    double *P = new double[ N2 ];
    double scale = -1.0/(double)m_numInds;
    for ( unsigned int i=0; i < N2; i++ )
    {
        P[ i ] = scale;
    }
    for ( int i=0; i<m_numInds; i++ )
    {
        P[ m_numInds*i + i ] += 1.0;
    }
    double alpha = 1.0, beta = 0.0;
    int one = 1;

    
    double *UPU = new double[ m_numInds*m_numInds ];
    double nd = 1.0 / ( (double)m_numInds - 1.0 );
    double *Ptemp = new double[ m_numInds ];
    
    if ( !isLM )
    {
        double *Pt = new double[  m_numInds*m_numInds ];
        dgemm( "N", "N", &m_numInds, &m_numInds, &m_numInds, &alpha, lmm->getU(), &m_numInds,  P, &m_numInds, &beta, Pt, &m_numInds ); 
        dgemm( "N", "T", &m_numInds, &m_numInds, &m_numInds, &alpha, Pt, &m_numInds,  lmm->getU(), &m_numInds, &beta, UPU, &m_numInds ); 
        delete [] Pt;

        /*
         * First recalculate the variance of each SNP
         */

        for ( unsigned int i = 0;  i < m_numSNPs; i++ )
        {   
            dgemm( "N", "N", &m_numInds, &one, &m_numInds, &alpha, UPU, &m_numInds,  &snpMatrix[ i*m_numInds], &m_numInds, &beta, Ptemp, &m_numInds ); 
            reader->setSNPvar( i, nd*ddot( &m_numInds, Ptemp, &incx, &snpMatrix[ i*m_numInds], &incx ) );
        }
    }
    list< unsigned int >::iterator itCand, itTag;
    for ( itTag = m_tagSNPs->begin(); itTag != m_tagSNPs->end(); itTag++ )
    {
        long posA( reader->getSNPpos( *itTag ) );
        if ( isLM )
        {
            dgemm( "N", "N", &m_numInds, &one, &m_numInds, &alpha, P, &m_numInds,  &snpMatrix[ (*itTag)*m_numInds], &m_numInds, &beta, Ptemp, &m_numInds );  
        }
        else
        {
            dgemm( "N", "N", &m_numInds, &one, &m_numInds, &alpha, UPU, &m_numInds,  &snpMatrix[ (*itTag)*m_numInds], &m_numInds, &beta, Ptemp, &m_numInds );  
        }

        if ( !m_proxyPairs.count( *itTag ) || !m_proxyPairs[ *itTag ]->size() )
        {
            continue;
        }

        //for ( itCand = m_candidateSNPs->begin(); itCand != m_candidateSNPs->end(); itCand++ )
        for ( itCand = m_proxyPairs[ *itTag ]->begin(); itCand != m_proxyPairs[ *itTag ]->end(); itCand++ )
        {
            long posB( reader->getSNPpos( *itCand ) );
            double r( nd*ddot( &m_numInds, &snpMatrix[ (*itCand)*m_numInds ], &incx, Ptemp, &incx )  / sqrt( reader->getSNPvar( *itCand) * reader->getSNPvar( *itTag ) ) );
            if ( fabs(r) > 0.98 )
            {
                r *= 0.98;
            }
            if ( fabs(r) > m_RMIN )
            {
                m_corrMatrix->at( *itCand ).insert(  snpLD( *itTag, r, &m_snpLabel[ *itTag ] ) );
                m_corrMatrix->at( *itTag ).insert(   snpLD( *itCand, r, &m_snpLabel[ *itCand ] ) );
            }
        }
    }
    delete [] P;
    delete [] UPU;
    delete [] Ptemp;
    /*
     * VARIOUS CHECKS ON PROXY SNP CONDITIONS
     * IF A SNP IS UNCONNECTED MAKE IT A PROXY
     */
    /*
    for ( unsigned int i=0; i < m_numSNPs; i++ )
    {
        if ( !m_snpLabel[i] && !m_corrMatrix->at(i).size() )
        {
            m_tagSNPs->push_back( i ); // ADD TO TAGS
            m_candidateSNPs->remove( i ); // REMOVE FROM CANDIDATES
            m_snpLabel[ i ] = 1; // SET IT TO TAG
        }
    }
    */
    
    /*
     * DISCRETIZE STATISTICS AND CORRELATIONS
     * SO TO HAVE FAST LOOKUP 
     */

    m_sGrid = 512;
    m_rGrid = 128;

    //m_sGrid = 2;
    //m_rGrid = 2;
    

    int sLen = 7*m_sGrid;
    int rLen = 2*m_rGrid;

    m_tableEC.resize( sLen ); // This is filled once
    m_tableER.resize( sLen ); // This is filled once
    m_tableS.resize( rLen ); // This will be filled when gradient changes
    for ( int i=0; i < sLen; i++ )
    {
        m_tableEC[i].resize( rLen );
        m_tableER[i].resize( rLen );
    }

    double c = m_cs[0];
    double s, r;
    for (int si=0; si < sLen; si++ )
    {
        s = (double)si/(double)m_sGrid;
        for ( int ri=0; ri < rLen; ri++ )
        {
            r = -1.0 + (double)ri/(double)m_rGrid;
            m_tableEC[ si ][ ri ] = expectedCost( s, c, r);
            m_tableER[ si ][ ri ] = expectedRecall( s, c, r);
        }
    }

}


int bisectionSolver::runBatch( bool expedite )
{
    /*
     * Get rules in batches
     */

    /*
     * Make Copy of tag and candidate SNPs
     */
     
    list< unsigned int > tagCopy;
    list< unsigned int > candidateCopy;
    list< unsigned int >::iterator it;
    /*
    for( it = m_tagSNPs->begin(); it != m_tagSNPs->end() ; it++ )
    {
        tagCopy.push_back( *it );
    }
    for( it = m_candidateSNPs->begin(); it != m_candidateSNPs->end() ; it++ )
    {
        candidateCopy.push_back( *it );
    }
    */
    m_proxyRules.resize( m_tagSNPs->size() );
    vector< unsigned int > proxyNo(m_numSNPs, 0);
    int no = 0;
    for ( it = m_tagSNPs->begin(); it != m_tagSNPs->end(); it++ )
    {
        proxyNo[ *it ] = no;
        m_proxyNo_ID.push_back( make_pair( no, *it) );
        no++;
    }
    /*
    m_tagSNPs->clear();
    m_candidateSNPs->clear();
    */
    list< decisionRule >::iterator pit;
    /*
    int count = 0;
    list< unsigned int >::iterator cit;
    for ( it=tagCopy.begin(); it != tagCopy.end(); it++ )
    {
        m_tagSNPs->push_back( *it );
        count++;
        if ( m_proxyPairs[ *it ]->size() )
        {
            for ( cit = m_proxyPairs[ *it ]->begin(); cit != m_proxyPairs[*it]->end(); cit++ )
            {
                m_candidateSNPs->push_back( *cit );
            }
        }
        
        if ( count == 10000 )
        {
            if ( m_solution ) { m_solution->clear(); }
            run( expedite );
            for ( pit = m_solution->begin(); pit != m_solution->end(); pit++ )
            {
                m_proxyRules[ proxyNo[ pit->tag ] ].push_back( testRule( pit->candidate, pit->s) );
            }
            m_tagSNPs->clear();
            m_candidateSNPs->clear();
            count = 0;
        }
    }
    */
    int count = 1;
    if ( count )
    {
        if ( m_solution ) { m_solution->clear(); }
        run( expedite );
        for ( pit = m_solution->begin(); pit != m_solution->end(); pit++ )
        {
            m_proxyRules[ proxyNo[ pit->tag ] ].push_back( testRule( pit->candidate, pit->s) );
        }
        m_tagSNPs->clear();
        m_candidateSNPs->clear();
        count = 0;
    }

    for ( int i=0; i<m_proxyRules.size(); i++ )
    {
        sort( m_proxyRules[i].begin(), m_proxyRules[i].end(), testRuleCmpStat() );  
    }
    return 1;
}

bisectionSolver::~bisectionSolver()
{
    if ( m_cs ) delete [] m_cs;
    if ( m_costProgression) { m_costProgression->clear(); delete m_costProgression;}
    if ( m_recallRateProgression) { m_recallRateProgression->clear(); delete m_recallRateProgression;}
    if ( m_gradientProgression) { m_gradientProgression->clear(); delete m_gradientProgression;}
    if ( m_solution ) { m_solution->clear(); delete m_solution; }
    if ( m_corrMatrix ) { m_corrMatrix->clear(); delete m_corrMatrix; }
    /*
     * Clean proxyPairs
     */
    list< unsigned int >::iterator itTag;
    for ( itTag = m_tagSNPs->begin(); itTag != m_tagSNPs->end(); itTag++ )
    {
        if ( m_proxyPairs[ *itTag ]->size() ) { delete m_proxyPairs[ *itTag ]; }
    }


}

double bisectionSolver::powerFunction(double ncp)
{
    double y[2];
    double a[2] = { -m_THRHLD - ncp, m_THRHLD - ncp};
    vdCdfNorm(2, a, y);
    return 1.0 - y[1] + y[0];
}



double bisectionSolver::getThreshold(double alpha)
{
    double significanceThreshold = 1.0 - 0.5*alpha;
    vdCdfNormInv(1, &significanceThreshold, &significanceThreshold);
    return significanceThreshold;
}

double bisectionSolver::expectedCost(double s, double c, double r )
{
    double p[3];
    double ss[3];

    ss[0] = (-s-r*m_NCP);
    ss[1] = (s-r*m_NCP);
    ss[2] = -s;

    pnormVec( ss, p, 3);
    return  c*( p[0] + 1.0 - p[1] ) + 2.0*(1.0-c)*p[2];
}


double bisectionSolver::expectedRecall(double s, double c, double r)
{
    double causal, noncausal;
    causal = computeQuadrantProb(s, m_THRHLD, r*m_NCP, m_NCP, r);
    noncausal = computeQuadrantProb(s, m_THRHLD, 0, 0, r);
    return c*causal + (1.0-c)*noncausal;
}



double bisectionSolver::expectedCostDerivative(double s, double c, double r)
{
    double ss[3];
    double d[3]; 

    ss[0] = (s-r*m_NCP);
    ss[1] = (s+r*m_NCP);
    ss[2] = s;

    dnormVec(ss, d, 3);
    return -c*( d[0] + d[1] ) - 2.0*(1-c)*d[2];
}


double bisectionSolver::expectedRecallDerivative(double s, double c, double r)
{
    double omr2 = 1.0 - pow(r,2);
    double somr2 = sqrt( omr2 );
    double ss[6];
    double p[6];

    ss[0] = (-m_THRHLD - omr2*m_NCP + r*s)/somr2;
    ss[1] = (m_THRHLD - omr2*m_NCP + r*s)/somr2;
    ss[2] = (-m_THRHLD - omr2*m_NCP - r*s)/somr2;
    ss[3] = (m_THRHLD - omr2*m_NCP - r*s)/somr2;
    ss[4] = (-m_THRHLD + r*s)/somr2;
    ss[5] = (m_THRHLD - r*s)/somr2;

    pnormVec(ss, p, 6);
    
    double scale1 = exp( -0.5*pow( s+r*m_NCP, 2.0) );
    double scale2 = exp( -0.5*pow( s-r*m_NCP, 2.0) );
    double scale3 = exp( -0.5*pow( s, 2.0) );

    double p1 = c*scale1*( p[0] + 1.0 - p[1] );
    double p2 = c*scale2*( p[2] + 1.0 - p[3] );
    double p3 = 2.0*(1-c)*scale3*( p[4] + 1.0 - p[5] );
    
    return -(p1+p2+p3)/SQRT2PI;
}


double bisectionSolver::totalExpectedCost(const list< decisionRule > *pairs)
{
    double total = 0.0;
    list< decisionRule >::const_iterator it;
    for ( it=pairs->begin(); it != pairs->end(); it++)
    {
        total += expectedCost( it->s, it->candidatePR, it->r);
    }
    return total;
}

double bisectionSolver::totalExpectedRecall(const list< decisionRule > *pairs)
{
    double total = 0.0;
    list< decisionRule >::const_iterator it;
    for (it=pairs->begin(); it != pairs->end(); it++)
    {
        total += expectedRecall( it->s, it->candidatePR, it->r);
    }
    return total;
}

double bisectionSolver::bisection( MemFn f, double c, double r, double targetValue, const double* interval, double tol)
{
    double l=interval[0];
    double u=interval[1];
    if ( ( (this->*f) (l,c,r) - targetValue ) * ( (this->*f) (u,c,r) - targetValue ) > 0 )
    {
        //cerr << "bisection: Not OPP!! ";
        //cerr << "l: " << l << " u: " << u;
        //cerr << " targetValue: " << targetValue << " ";
        //cerr << " val@l: " << (this->*f) (l,c,r)  << " val@u: " <<  (this->*f) (u,c,r) << endl; 
        return l;
    }
    double mid = (this->*f) ( 0.5*(l+u),c,r) - targetValue;
    int i = 1;
    while( fabs(mid) > tol && i < m_NMAX )
    {
        if ( mid * ( (this->*f) (l,c,r) - targetValue ) > 0 )
        {
            l = 0.5*(l+u);
        }
        else
        {
            u = 0.5*(l+u);
        }
        mid = (this->*f) ( 0.5*(l+u),c,r) - targetValue;
        i++;
    }
    return 0.5*(l+u);
} 
double bisectionSolver::bisectionVec( MemFnVec f, list< decisionRule > *pairs, double targetValue, const double* interval, double tol)
{
    double l=min( interval[0], interval[1] );
    double u=max( interval[0], interval[1] );
    double lvalue (  (this->*f) (l,pairs,interval) - targetValue );
    double uvalue (  (this->*f) (u,pairs,interval) - targetValue ); 
    if ( fabs( lvalue ) < m_TOL )
    {
        return l;
    }
    
    if ( fabs( uvalue ) < m_TOL )
    {
        
        return u;
    }

    if ( lvalue * uvalue > 0 )
    {
        //cerr << "bisectionVec: Not OPP!!" << endl;
        //cerr << lvalue << " " << uvalue << endl;
        return l;
    }
    double mid = (this->*f) ( 0.5*(l+u),pairs,interval) - targetValue;
    int i = 1;
    while( fabs(mid) > tol && i < m_NMAX )
    {
        if ( mid * ( (this->*f) (l,pairs,interval) - targetValue ) > 0 )
        {
            l = 0.5*(l+u);
        }
        else
        {
            u = 0.5*(l+u);
        }
        mid = (this->*f) ( 0.5*(l+u),pairs,interval) - targetValue;
        i++;
    }
    return 0.5*(l+u);
} 

double bisectionSolver::mapS2LogGradient(double s, double c, double r)
{
    return log( -expectedCostDerivative(s,c,r) ) - log( -expectedRecallDerivative(s,c,r) );

}

double bisectionSolver::mapLogGradient2S(double logGradient, double c,double r, const double* interval)
{
    //double s = bisection( &bisectionSolver::mapS2LogGradient, c, r, logGradient, interval, 1e-5*fabs(logGradient) );
    double s = bisection( &bisectionSolver::mapS2LogGradient, c, r, logGradient, interval, 1e-3 );
    return s;
}

double bisectionSolver::mapLogGradient2TotalER(double logGradient, list< decisionRule > *pairs, const double* interval)
{
    updateLookUp( logGradient );
    double totalER = 0.0;
    double tempInt[2] = {0.0, 7.0};
    list< decisionRule >::iterator it;
    for ( it=pairs->begin(); it != pairs->end(); it++)
    {
        it->s = mapLogGradient2S( logGradient, it->candidatePR, it->r, tempInt );
        //totalER += expectedRecall(it->s, it->candidatePR, it->r);
        totalER += lookUpER( it->s, it->r ); 
    }
    return totalER;
}


double bisectionSolver::mapLogGradient2TotalEC(double logGradient, list< decisionRule > *pairs, const double * interval)
{
    double totalEC = 0.0;
    double tempInt[2] = {0.0, 7.0};
    list<decisionRule>::iterator it;
    for ( it=pairs->begin(); it != pairs->end(); it++)
    {
        it->s = mapLogGradient2S( logGradient, it->candidatePR, it->r, tempInt );
        //totalEC += expectedCost(it->s, it->candidatePR, it->r );
        totalEC += lookUpEC( it->s, it->r );
    }
    return totalEC;
}

void bisectionSolver::mapLogGradient2TotalPerf(double logGradient, list< decisionRule > *pairs, double &totalEC, double &totalER)
{
    totalEC = 0.0;
    totalER = 0.0;
    double tempInt[2] = {0.0, 7.0};
    list< decisionRule>::iterator it;
    for ( it=pairs->begin(); it != pairs->end(); it++)
    {
        it->s = mapLogGradient2S( logGradient, it->candidatePR, it->r, tempInt ) ;
        totalEC += expectedCost(it->s, it->candidatePR, it->r );
        totalER += expectedRecall(it->s, it->candidatePR, it->r );
        //totalER += lookUpER( it->s, it->r ); 
        //totalEC += lookUpEC( it->s, it->r ); 

    }
}


void bisectionSolver::mapLogGradient2TotalPerfLookUp(double logGradient, list< decisionRule > *pairs, double &totalEC, double &totalER)
{
    totalEC = 0.0;
    totalER = 0.0;
    list< decisionRule>::iterator it;
    for ( it=pairs->begin(); it != pairs->end(); it++)
    {
        it->s = m_tableS[ floor( (1.0 + it->r)*(double)m_rGrid ) ];
        totalER += lookUpER( it->s, it->r ); 
        totalEC += lookUpEC( it->s, it->r ); 
    }
}


int bisectionSolver::solverX( list<decisionRule> *pairs, double targetER, double &ec, double &er, double gr)
{
    mapLogGradient2TotalPerfLookUp( gr, pairs, ec, er);
    return 0;
}


void bisectionSolver::updateLookUp(double gradient)
{
    double tempInt[2] = {0.0, 7.0};
    double r;
    for ( int ri=0; ri < 2*m_rGrid; ri++ ) // CACHE THE VALUES FOR THE CURRENT GRADIENT
    {
        r = (double)ri/(double)m_rGrid - 1.0;
        m_tableS[ ri ] = mapLogGradient2S( gradient, m_cs[0], r, tempInt );
    }
}


int bisectionSolver::solver( list<decisionRule> *pairs, double targetER, double &ec, double &er, double &gr)
{
    double interval[2];

    // Find the search range for the log gradient    
    double gl;
    double gu;
    double temp1, temp2;
    double res;
    list<decisionRule>::iterator it;
    
    /*
    double minR = 10.0;    
    for ( it=pairs->begin(); it!=pairs->end(); it++)
    {
        cout << " " << it->r;
    }
    cout << endl;
    //gl = mapS2LogGradient( 0.0, m_cs[0], minR );
    //gu = mapS2LogGradient( 7.0, m_cs[0], minR );
    */
    gl = 60.0;
    gu = 10.0;

    double maxER;
    double maxEC;

    double minER;
    double minEC;
    updateLookUp( gl );
    //mapLogGradient2TotalPerf(gl, pairs, maxEC, maxER );
    mapLogGradient2TotalPerfLookUp( gl, pairs, maxEC, maxER );

    updateLookUp( gu );
    //mapLogGradient2TotalPerf(gu, pairs, minEC, minER );
    mapLogGradient2TotalPerfLookUp( gu, pairs, minEC, minER );
    if ( maxER < targetER )
    {
        res = gl;
    }
    else if ( minER > targetER )
    {
        res = gu;
    }
    else if ( fabs( gu - gl ) < m_TOL )
    {

        res = gl;
    }
    else
    {
        interval[0] = gl;
        interval[1] = gu;
        res = bisectionVec( &bisectionSolver::mapLogGradient2TotalER, pairs, targetER, interval, 1e-4*targetER );
    }
    
    gr = res;
    ec = 0.0;
    er = 0.0;
    mapLogGradient2TotalPerf( res, pairs, ec, er);
    return 1;
}

double bisectionSolver::getCorr( unsigned int a, unsigned int b)
{
    return m_corrMatrix->at(a).find(b)->r;
}

void bisectionSolver::updateTags( const vector< unsigned int> *newTags, list< unsigned int > *candList, list< unsigned int > *tagList )
{
    if ( !newTags || !tagList || !candList || !candList->size() )
    {
        cerr << "updateTags: Error!" << endl;
        return;
    }
    // Using the new tags update tags.
    vector< unsigned int>::const_iterator vt;
    for (vt=newTags->begin(); vt!=newTags->end(); vt++)
    {
        m_snpLabel[ *vt ] = 1;
        tagList->push_back( *vt );
        candList->remove( *vt );
    }
}

void bisectionSolver::undoTags( const vector< unsigned int> *removeTags, list< unsigned int > *candList, list< unsigned int > *tagList )
{
    
    if ( !removeTags || !tagList || !candList || !tagList->size() )
    {
        cerr << "undoTags: Error!" << endl;
        return;
    }

    // These tags are now being removed from the best tag look-up
    // add them 
    vector< unsigned int >::const_iterator vit;
    for (vit=removeTags->begin(); vit != removeTags->end(); vit++ )
    {
        m_snpLabel[ *vit ] = 0;
        tagList->remove( *vit );
        candList->push_back( *vit );
    }
}




void bisectionSolver::getBestTagPairs(const list<unsigned int> *candidates, list<decisionRule> *pairs)
{
    if ( !candidates || !pairs )
    {
        cerr << "getBestTagPairs: NULL pointer" << endl;
    }

    pairs->clear();
    list< unsigned int >::const_iterator itCand;
    setBestTag::iterator itTag;    
    for ( itCand=candidates->begin(); itCand != candidates->end(); itCand++)
    {
        for ( itTag=m_corrMatrix->at( *itCand ).begin(); itTag != m_corrMatrix->at( *itCand ).end(); itTag++)
        {
            if ( *(itTag->isTag) )
            {
                pairs->push_back(
                    (decisionRule)
                    {
                        *itCand, // candidate
                        itTag->index, // best-tag
                        0.0, // s
                        itTag->r, // correlation
                        m_cs[ *itCand ], // candidate prior
                        0.0, // for now I don't use tag prior
                        0.0, // expectedCost
                        0.0 // expected Recall
                    }
                );
                break;
            }
        }
    }
} 



void bisectionSolver::fixTags( vector<unsigned int> *newTags, list< unsigned int> *candList,  list< unsigned int> *tagList )
{
    /* WE HAVE WORKING LISTS FOR TAGS AND CANDIDATES
     * IF A CANDIDATE IS NOT TAGGED MAKE IT A TAG
     * REMEMBER WHICH SNPS BECAME A TAG IN THE NEWTAGS ARRAY 
     */


    if ( !newTags || !candList || !tagList )
    {
        cerr << "fixTags: Error!" << endl;
        return;
    }
    
    /*
     * FIXES THE TAG LABELS
     * IF A SNP IS NOT TAGGED IT SHOULD BE A TAG
     * THIS MAY HAPPEN WHEN WE REMOVE A CANDIDATE AND ADD IT BACK
     */


    list< unsigned int>::iterator it;
    setBestTag::iterator sit;
    
    /*     
    vector< char > isTagged ( m_numSNPs, 0 ); 
    for ( it=tagList->begin(); it!=tagList->end(); it++)
    {
        isTagged[ *it ] = 1;
        for ( sit=m_corrMatrix->at( *it ).begin(); sit != m_corrMatrix->at( *it ).end(); sit++ )
        {
            isTagged[ sit->index ] = 1;
        }
    }

    for ( unsigned long i = 0; i < m_numSNPs; i++ )
    {
        if ( isTagged[ i ] == 0 )
        {
            newTags->push_back( i );
            candList->remove( i );
            tagList->push_back( i );
            m_snpLabel[ i ] = 1;
        }
    }

    */
    unsigned long size;
    vector< unsigned int > memory;
    bool hasTag;
    do
    {
        size = newTags->size();
        memory.clear();
        for ( it=candList->begin(); it!=candList->end(); it++)
        {
            hasTag = false; // DOES THIS SNP HAVE A PROXY?
            for ( sit=m_corrMatrix->at( *it ).begin(); sit != m_corrMatrix->at( *it ).end(); sit++ )
            {
                // AMONG ALL ITS CORRELATIONS CHECK IF ANY ONE IS A TAG
                hasTag |= *(sit->isTag);
                if ( hasTag ) break;
            }
            
            //
            // IT DOESN'T HAVE A PROXY, MAKE IT ONE. 
            // THIS IS A SOFT CASE, WE DON'T APPLY TO WORKING CANDIDATE TAG ARRAYS YET. 
            // STORE IN A MEMORY AND DO AT THE END
            // 

            if ( !hasTag )
            {
                m_snpLabel[ *it ] = 1; // MAKE IT A PROXY
                memory.push_back( *it ); // REMEMBER
                break;
            }
        }

        for ( vector< unsigned int>::iterator i=memory.begin(); i!= memory.end(); i++)
        {
            newTags->push_back( *i );
            candList->remove( *i );
            tagList->push_back( *i );
        }
    } while( size != newTags->size() ); // repeat while there are no more changes.
    // I don't update the tag List because later we might undo these tags. So I need to remember them in the vector.

}


void bisectionSolver::printBestTagTable( list< unsigned int> *candidates)
{
    list<unsigned int>::const_iterator it;
    setBestTag::iterator sit;

    cout << "#Cands: " << candidates->size() << endl;

    for ( it=candidates->begin(); it != candidates->end(); it++)
    {
        for ( sit=m_corrMatrix->at( *it ).begin(); sit != m_corrMatrix->at( *it ).end(); sit++)
        {
            if ( *(sit->isTag) )
            {
                cerr << *it << " <---> " << sit->index << endl;
                break;
            }
        }
    }
    cerr << endl;
}

int bisectionSolver::run( bool expedite )
{

    list< unsigned int > *tempCandidates = new list< unsigned int>;
    list< unsigned int > *tempTags = new list< unsigned int>;
    list< unsigned int > *iterationBestCandidates = new list< unsigned int>;
    list< unsigned int > *iterationBestTags = new list< unsigned int >;
    list< unsigned int >::iterator it;
    
    
    for ( it=m_candidateSNPs->begin(); it != m_candidateSNPs->end(); it++)
    {
        iterationBestCandidates->push_back(*it);
        tempCandidates->push_back(*it);
    }
    for ( it=m_tagSNPs->begin(); it != m_tagSNPs->end(); it++ )
    {
        iterationBestTags->push_back( *it );
        tempTags->push_back( *it );
    }
    clock_t clock_begin=clock();

    // This will store the SNP pairs.
    list< decisionRule > *pairs = new list< decisionRule >;
    list< decisionRule > *iterationBestPairs = new list< decisionRule >;
    list< decisionRule >::iterator pit;
    if ( !m_solution )
    {
        m_solution =  new list< decisionRule >;
    }
    else
    {
        m_solution->clear();
    }

    unsigned int BATCH_m_numSNPs = m_tagSNPs->size() + m_candidateSNPs->size();

    double scaler = m_cs[0]*m_POWER + (1.0 - m_cs[0] )*m_ALPHA;
    double targetER =  (m_SENSITIVITY*(double)BATCH_m_numSNPs - (double)m_tagSNPs->size() )*scaler;
    //double targetER = (m_sensitivity*(double)m_candidateSNPs->size()*scaler - m_tagSNPs->size()*m_alpha);

    double recallAtminCost, gradientAtminCost;
    double totalEC, totalER, currentGradient;

    m_costProgression = new vector< double >;
    m_recallRateProgression = new vector< double >;
    m_gradientProgression = new vector<double>;

    if ( targetER < m_TOL )
    {
        // I'm already above the asked sensitivity
        cerr << "Current set of proxies meets the level: "  << m_SENSITIVITY << endl; 
        getBestTagPairs( m_candidateSNPs, m_solution );
        solver( m_solution, targetER, totalEC, totalER, currentGradient);
        double currentSensitivity = ((double)m_tagSNPs->size()*scaler + totalER)/( (double)BATCH_m_numSNPs*scaler ) ;
        //double currentSensitivity = (  (double)m_tagSNPs->size()*m_alpha + totalER )/ (double)m_candidateSNPs->size()*scaler;
        m_costProgression->push_back( totalEC + m_tagSNPs->size() );
        m_recallRateProgression->push_back( currentSensitivity );
    }
    else
    {
        // This is the initial candidate / tag setting
        double iterationMinCost = (double)BATCH_m_numSNPs + 1.0;
        double globalMinCost = (double)BATCH_m_numSNPs + 2.0;
        double currentSensitivity;
        double iterationSensitivity;
        vector< unsigned int> *moveTags = new vector< unsigned int >; // This is conversion memory
        vector< unsigned int>::iterator mit;
        unsigned int minCostTagIndex, currentIndex;
        

        cout << "#SNPs: " << BATCH_m_numSNPs
            << " #Proxies: " << m_tagSNPs->size()
            << " #Remainders: " << m_candidateSNPs->size()
            << endl;

        cerr.precision(5);
        cerr << "#Proxies\tCost\tRecallRate\tgradient\ttime" << endl;
        bool isFirstRound = true;
        /*
         * IF WE HAVE PROXIES SET THE BEST TAG ASSIGNMENTS
         */

        if ( ( expedite && m_tagSNPs->size() ) || m_fixedProxies )
        {
            moveTags->clear();
            fixTags( moveTags, tempCandidates, tempTags ); // THIS UPDATED THE TAGS AND THE CANDIDATES
            getBestTagPairs( tempCandidates, m_solution );
            solver( m_solution, targetER, totalEC, totalER, currentGradient); // WE SOLVED FROM SCRATCH
            isFirstRound = false;
            currentSensitivity =  ((double)tempTags->size()*scaler + totalER)/( (double)BATCH_m_numSNPs*scaler ) ;
            if ( !m_fixedProxies )
            {
                double tempInt[2] = {0.0, 7.0};
                double r;
                for ( int ri=0; ri < 2*m_rGrid; ri++ ) // CACHE THE VALUES FOR THE CURRENT GRADIENT
                {
                    r = (double)ri/(double)m_rGrid - 1.0;
                    m_tableS[ ri ] = mapLogGradient2S( currentGradient, m_cs[0], r, tempInt );
                }
            }
            if ( m_fixedProxies )
            {
                m_costProgression->push_back(  totalEC + tempTags->size() );
                m_recallRateProgression->push_back( currentSensitivity );
                m_gradientProgression->push_back( currentGradient );
                cerr << tempTags->size() <<"\t" << m_costProgression->back() << "\t" << m_recallRateProgression->back() << "\t" << m_gradientProgression->back() << endl;
            }
        }
        /*
         * IF NOT GIVEN FIXED PROXIES OR 
         * GIVEN FIXED PROXIES AND GROW FROM THEM
         */

        while( ( !m_fixedProxies || m_isProxyStart ) && ( tempTags->size() < (BATCH_m_numSNPs) ) )
        {
            
            if ( tempTags->size() + tempCandidates->size() != BATCH_m_numSNPs )
            {
                cerr << "#Proxies + #Candidates != #SNPs" << endl;
                abort();
            }
            
            // Move candidates into a stack.           
            stack< unsigned int > testCandidates;
            for (it=tempCandidates->begin(); it!=tempCandidates->end(); it++)
            {
                testCandidates.push( *it );
            }
           
            clock_t innerWhileClock_begin = clock(); 
            iterationMinCost = BATCH_m_numSNPs;
            while( !testCandidates.empty() )
            {
                /* Test this candidate as a tag
                 */
                moveTags->clear();
                currentIndex = testCandidates.top();
                testCandidates.pop();
                moveTags->push_back( currentIndex );
                updateTags( moveTags, tempCandidates, tempTags);
                fixTags( moveTags, tempCandidates, tempTags );
                targetER =  (m_SENSITIVITY*(double)BATCH_m_numSNPs - (double)tempTags->size() )*scaler;
                //targetER = m_sensitivity*(double)tempCandidates->size()*scaler - (double)tempTags->size()*m_alpha;
                getBestTagPairs( tempCandidates, pairs );
                
                if ( !expedite || isFirstRound )
                {
                    isFirstRound = false;
                    solver( pairs, targetER, totalEC, totalER, currentGradient);
                    if ( expedite )
                    {
                        double tempInt[2] = {0.0, 7.0};
                        double r;
                        for ( int ri=0; ri < 2*m_rGrid; ri++ )
                        {
                            r = (double)ri/(double)m_rGrid - 1.0;
                            m_tableS[  ri ] = mapLogGradient2S( currentGradient, m_cs[0], r, tempInt );
                            //cout << "r: " << r << " s: " << m_tableS[ ri ] << endl;
                        }
                    }
                }
                else
                {
                    solverX( pairs, targetER, totalEC, totalER, currentGradient );
                }
                
                totalEC += tempTags->size();
                if ( totalEC < iterationMinCost )
                {
                    minCostTagIndex = currentIndex;
                    iterationMinCost = totalEC;
                    recallAtminCost = totalER;
                    gradientAtminCost = currentGradient;

                    iterationBestPairs->clear();
                    for (pit=pairs->begin(); pit!=pairs->end(); pit++)
                    {
                        iterationBestPairs->push_back( *pit );
                    }
                    //iterationSensitivity =  ((double)tempTags->size()*scaler + recallAtminCost)/( (double)m_numSNPs*scaler );
                    
                }
                /* Undo the changes
                 */
                undoTags( moveTags, tempCandidates, tempTags );
            
            } // end of inner while


            clock_t innerWhileClock_end = clock();
            
            moveTags->clear();
            moveTags->push_back( minCostTagIndex );

            updateTags( moveTags, tempCandidates, tempTags);
            fixTags( moveTags, tempCandidates, tempTags );
            
            iterationBestTags->clear();
            for ( it=tempTags->begin(); it != tempTags->end(); it++)
            {
                iterationBestTags->push_back( *it );
            }
            
            iterationBestCandidates->clear();
            for ( it=tempCandidates->begin(); it != tempCandidates->end(); it++)
            {
                iterationBestCandidates->push_back( *it );
            }
            iterationSensitivity =  ((double)tempTags->size()*scaler + recallAtminCost)/( (double)BATCH_m_numSNPs*scaler );
            
            //if ( expedite && ( iterationSensitivity > m_sensitivity )  )
            {
                //cerr << "Solving" << endl;    
                double previousGradient = currentGradient;
                targetER =  (m_SENSITIVITY*(double)BATCH_m_numSNPs - (double)iterationBestTags->size() )*scaler;
                getBestTagPairs( iterationBestCandidates, iterationBestPairs ); 
                solver( iterationBestPairs, targetER, totalEC, totalER, currentGradient); 
                recallAtminCost = totalER;
                iterationMinCost = totalEC + iterationBestTags->size();

                if ( fabs( currentGradient - previousGradient ) > 0.1 )
                {
                    // Fill the tableS for quick look-up
                    double tempInt[2] = {0.0, 7.0};
                    double r;
                    for ( int ri=0; ri < 2*m_rGrid; ri++ )
                    {
                        r = (double)ri/(double)m_rGrid - 1.0;
                        m_tableS[  ri ] = mapLogGradient2S( currentGradient, m_cs[0], r, tempInt );
                    }
                }
            }

            currentSensitivity = ((double)iterationBestTags->size()*scaler + recallAtminCost)/( (double)BATCH_m_numSNPs*scaler );
            //currentSensitivity = ((double)m_tagSNPs->size()*m_alpha + recallAtminCost)/( (double)m_candidateSNPs->size()*scaler ) ;
            m_costProgression->push_back( iterationMinCost );
            m_recallRateProgression->push_back( currentSensitivity );
            m_gradientProgression->push_back( gradientAtminCost );
            cerr << iterationBestTags->size() << "\t" << m_costProgression->back() << "\t" << m_recallRateProgression->back() << "\t" << m_gradientProgression->back();
            cerr << "\t" << double(diffclock(innerWhileClock_end, innerWhileClock_begin)) << " secs."<< endl;


            if (  currentSensitivity < (m_SENSITIVITY - 1e-4) )
            {
                continue;
            }
            else
            {
                if ( iterationMinCost > globalMinCost ) 
                {
                    
                    m_candidateSNPs->clear();
                    for ( it=iterationBestCandidates->begin(); it != iterationBestCandidates->end(); it++ )
                    {
                        m_candidateSNPs->push_back( *it );
                        m_snpLabel[ *it ] = 0;
                    }
                    m_tagSNPs->clear();
                    for ( it=iterationBestTags->begin(); it != iterationBestTags->end(); it++ )
                    {
                        m_tagSNPs->push_back( *it );
                        m_snpLabel[ *it ] = 1;
                    }
                    
                    m_solution->clear();
                    for (pit=iterationBestPairs->begin(); pit!=iterationBestPairs->end(); pit++)
                    {
                        if ( fabs( pit->s ) < 1e-2 )
                        {
                            m_candidateSNPs->remove( pit->candidate );
                            m_tagSNPs->push_back( pit->candidate );
                            m_snpLabel[ pit->candidate ] = 1;
                        }
                        else
                        {
                            m_solution->push_back( *pit );
                        }

                    }
                    
                    m_costProgression->pop_back();
                    m_recallRateProgression->pop_back();
                    m_gradientProgression->pop_back();
                    break;
                }
                else
                {
                    globalMinCost = iterationMinCost;
                    m_solution->clear();
                    for (pit=iterationBestPairs->begin(); pit!=iterationBestPairs->end(); pit++)
                    {
                        m_solution->push_back( *pit );
                    }
                    m_candidateSNPs->clear();
                    for ( it=iterationBestCandidates->begin(); it != iterationBestCandidates->end(); it++ )
                    {
                        m_candidateSNPs->push_back( *it );
                        m_snpLabel[ *it ] = 0;
                    }
                    m_tagSNPs->clear();
                    for ( it=iterationBestTags->begin(); it != iterationBestTags->end(); it++ )
                    {
                        m_tagSNPs->push_back( *it );
                        m_snpLabel[ *it ] = 1;
                    }
                }
            }
        } // end of main while 
        delete moveTags;
    
        clock_t clock_end=clock();
        cout << "Solver runtime: " << double(diffclock(clock_end,clock_begin)) << " sec."<< endl;
        
        if ( m_costProgression && m_costProgression->size() ) m_minExpectedCost = m_costProgression->back();
        if ( m_recallRateProgression && m_recallRateProgression->size() ) m_expectedRecallRate = m_recallRateProgression->back();

    }
    

    tempCandidates->clear();
    delete tempCandidates;

    tempTags->clear();
    delete tempTags;

    pairs->clear();
    delete pairs;
   
    iterationBestCandidates->clear();
    delete iterationBestCandidates;

    iterationBestTags->clear();
    delete iterationBestTags;

    iterationBestPairs->clear();
    delete iterationBestPairs;

    return 0;
}


void bisectionSolver::save()
{
    if ( !m_solution && m_tagSNPs->size() < m_numSNPs )
    {
        cerr << "save: Error! There is no solution yet" << endl;
        return;
    }

    list< unsigned int >::iterator it;
    list< decisionRule >::iterator pit;


    cerr << "m_numSNPs: " << m_numSNPs << endl;
    cerr << "#proxies: " << m_tagSNPs->size() << endl;
    cerr << "#cands: " << m_candidateSNPs->size() << endl;
    cerr << "#soln: " << m_solution->size() << endl;
    if ( m_candidateSNPs->size() != m_solution->size() )
    {
        cerr << "save: Error! Number mismatch!!" << endl;

    }


    /*
    ofstream fp( (m_outFilename + ".proxies").c_str() );
    for ( it = m_tagSNPs->begin(); it != m_tagSNPs->end(); it++ ) 
    {
        fp <<  getSNPName( *it ) << "\n";
    }
    fp.close();

    ofstream frules( (m_outFilename + ".rules").c_str() );
    if ( m_solution )
    {
        for ( pit=m_solution->begin(); pit!=m_solution->end(); pit++ )
        {
            frules  <<  getSNPName( pit->candidate )
                << "\t"
                <<  getSNPName( pit->tag )
                << "\t"
                << pit->s
                << "\t"
                << pit->r
                //<< ( 1.0 - p[i] )
                << "\n";
        }
    }
    frules.close();
    */
}

double bisectionSolver::diffclock(clock_t clock1,clock_t clock2)
{

    double diffticks=clock1-clock2;
	double sec=(diffticks)/CLOCKS_PER_SEC;
	return sec;
}

