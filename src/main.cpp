#include <boost/program_options.hpp>
#include <iostream>

#include "../include/methodBisection.h"
#include "../include/readTPED.h"
#include "../include/RoLMM_etsi.h"


using namespace std;
namespace po = boost::program_options;

int main(int ac, char* av[])
{
    string prefixFile, phenoFile, covariateFile, outPrefix, kinshipFile;
    covariateFile="";
    outPrefix = "";
    kinshipFile = "";
    bool isLM = true;
    bool isLMM = false;
    bool isREML=false;
    bool isIBS=false;
    bool isPlot = false;
    double pfilter = 1.0;
    double evThreshold = 1e-5;    
    string proxyFilename = "";
    string ruleFilename = "";
    double ALPHA=1e-8, POWER=0.5, SENSITIVITY=0.99, RMIN=0.2; 
    int MAXDISTANCE=5;
    double minMAF = 0.01;
    try 
    {
        po::options_description generic("Allowed options");
        generic.add_options()
            ("help,h", "help")

            ("proxies", po::value<string>(&proxyFilename),"filename for proxies")
            ("rules",   po::value<string>(&ruleFilename), "filename for pairs")
            
            ("power",   po::value<double>(&POWER),        "statistical power at the causal SNP, default: 0.5")
            ("alpha",   po::value<double>(&ALPHA),        "significance threshold, default: 1e-8")
            ("trr",     po::value<double>(&SENSITIVITY),  "target recall rate, default 0.99")
            ("rmin",    po::value<double>(&RMIN),         "min. pairwise |r|, default: 0.2")
            ("dmax",    po::value<int>(&MAXDISTANCE),     "max. distance (Mb) allowed to proxy, default: 5")
            ("maf",     po::value<double>(&minMAF),       "min. minor allele frequency, default: 0.01")

            ("tfile",   po::value<string>(&prefixFile),   "prefix for .tped and .tfam files")
            ("pheno",   po::value<string>(&phenoFile),    "phenotype file")
            ("covar",   po::value<string>(&covariateFile),"covariate file")
            ("out",     po::value<string>(&outPrefix),    "output file prefix")
            
            ("lmm",     "linear mixed-model")
            ("reml",    "REML vs. ML")
            ("kin",     po::value<string>(&kinshipFile),  "kinship file")
            ("pfilter", po::value<double>(&pfilter),      "filter results by p-value")
            ("plot",    "for plotting" )
        ;

        po::variables_map vm;
        po::store( po::parse_command_line( ac, av, generic), vm);
        po::notify(vm);
        
        if ( ac < 2 || vm.count("help") )
        {
            cout << generic << endl;
            return 1;
        }


        if ( !vm.count( "proxies" ) )
        {
            cerr << "Missing proxy file" << endl;
            abort();
        }



        /*
         * SETTING LM OR LMM flags
         */

        if ( vm.count( "lmm" ) )
        {
            isLMM = true;
            isLM = false;
        }

        if ( isLMM )
        {
            if ( !kinshipFile.size() )
            {
                cerr << "--lmm option requires a kinship file" << endl;
                abort();
            }
        }

        /*
         * REML vs. ML likelihood
         */
        if ( vm.count("reml" ) )        
        {
            if (  !isLMM )
            {
                cerr << "--reml: requires --lmm option" << endl;
            
            }
            else
            {
                isREML = true;
            }
        }

        if ( vm.count("plot"))
        {
            isPlot = true;
        }

    }
    catch( std::exception &e )
    {
        cerr << e.what() << endl;
    }

    if ( !outPrefix.size() )
    {
        outPrefix = prefixFile; 
    }
    
    /*
     * READ TPED, TFAM, PHENOTYPE AND COVARIATE FILES
     */

    readTPED *reader = new readTPED( prefixFile, phenoFile, covariateFile );
    if ( isLMM )
    {
        reader->readKinship( kinshipFile );
    }
    cerr << "passed reader" << endl;
    RoLMM *lmm = new RoLMM( reader->getNumInds(), reader->getNumCovs(), reader->getNumPhens(), outPrefix );
    lmm->setReader( reader );
    lmm->setPfilter( pfilter );
    if ( isLMM )
    {
        lmm->setKinship( reader->getKinship(), evThreshold );
    }
    lmm->setX( reader->getCovMatrix(), isLM );
    lmm->setY( reader->getPhenMatrix(), isLM );
    lmm->setSNPMatrix( reader->getSNPMatrix(), reader->getNumSNPs() ); 
    cerr << "set data" << endl;
    bisectionSolver *rules = new bisectionSolver( reader, lmm, isLM, proxyFilename, ruleFilename, ALPHA, POWER, SENSITIVITY, RMIN, MAXDISTANCE, minMAF );
    rules->runBatch( true );
    cerr << "about to test SNPs" << endl;
    if ( isLM )
    {
        lmm->testSNPsLM( rules );
    }
    else
    {
        lmm->testSNPsLMM(rules, isREML);
    }

    delete lmm;
    delete reader;
    delete rules;
    return 0;
}

