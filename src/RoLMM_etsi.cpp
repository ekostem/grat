#include "../include/RoLMM_etsi.h"

RoLMM::RoLMM(int numInds, int numCovs, int numPhens, string outPrefix):m_numInds( numInds ), m_numCovs( numCovs ), m_numPhens( numPhens )
{
    m_pvalueFilename = outPrefix + ".pvalues";
    m_infoFilename = outPrefix + ".info";

    m_reader = NULL;
    m_dist = NULL; 

    m_pfilter = 1.0; // Means store all pvalue results
    
    // Centering matrix 1/(n-1) ( I - 1/n 11^t )
    m_P = new double[ m_numInds*m_numInds]; 
    double n = (double)m_numInds;
    for ( int i=0; i<m_numInds*m_numInds; i++ ) m_P[i] = -1.0/( n );
    for ( int i=0; i<m_numInds; i++ ) m_P[i*m_numInds+i] += 1.0;
    

    m_K = NULL;
    m_S = NULL;
    m_U = NULL;


    m_Y = new double[ m_numInds*m_numPhens ];
    m_X = new double[ m_numInds*m_numCovs ];

    m_Xu = NULL;
    m_Xl = NULL;

    m_Yu = NULL;
    m_Yl = NULL;

    m_beta = NULL;
    m_dbeta = NULL;

    m_Xubeta = NULL;
    m_Xudbeta = NULL;

    m_Xlbeta = NULL;
    m_Xldbeta = NULL;

    m_XltYl = NULL;
    m_XltXl = NULL;


    m_XtX = new double[ m_numCovs*m_numCovs ];
    m_invXtX = new double[ m_numCovs*m_numCovs ];
    
    m_XltXl = new double[ m_numCovs*m_numCovs ];

    m_Xor = new double[ m_numInds*m_numCovs ];
    m_Yor = new double[ m_numInds ];
    
    m_beta = new double[ m_numCovs ];
    m_dbeta = new double[ m_numCovs ];
    
}

RoLMM::~RoLMM()
{

    if ( m_XltXl ) delete [] m_XltXl;
    if ( m_XltYl ) delete [] m_XltYl;
    

    if ( m_XtX ) delete [] m_XtX;
    if ( m_invXtX ) delete [] m_invXtX;

    if ( m_P ) delete [] m_P;
    
    if ( m_S ) delete [] m_S;
    if ( m_U ) delete [] m_U;

    if ( m_Y ) delete [] m_Y;
    if ( m_X ) delete [] m_X;

    if ( m_Xu ) delete [] m_Xu;
    if ( m_Xl ) delete [] m_Xl;

    if ( m_Yu ) delete [] m_Yu;
    if ( m_Yl ) delete [] m_Yl;
  
    if ( m_beta ) delete [] m_beta;
    if ( m_dbeta ) delete [] m_dbeta;
    
    if ( m_Xubeta ) delete [] m_Xubeta;
    if ( m_Xudbeta ) delete [] m_Xudbeta;

    if ( m_Xlbeta ) delete [] m_Xlbeta;
    if ( m_Xldbeta ) delete [] m_Xldbeta;
    if ( m_dist ) delete m_dist;
}


void RoLMM::setSNPMatrix( double *snpMatrix, int numSNPs )
{
    m_numSNPs = numSNPs;
    m_snpMatrix = snpMatrix;
}

void RoLMM::setKinship( double *K, double evThreshold )
{
    m_K = K;
    double *m_Utemp = new double[ m_numInds*m_numInds ];
    double *m_Stemp = new double[ m_numInds ];

    getEigenDecomposition( m_K, m_numInds, m_Utemp, m_Stemp );
    
    int incx = 0;
    m_up = 0;
    for ( int i=0; i<m_numInds; i++ )
    {
        if ( m_Stemp[i] > evThreshold )
        {
            m_up++;
        }
    }

    m_S = new double[ m_up ];
    m_U = new double[ m_numInds*m_up ];

    m_low = m_numInds - m_up;
    m_Xu = new double[ m_up*m_numCovs ];
    m_Yu = new double[ m_up ];
    
    if ( m_low )
    {
        m_Xl = new double[ m_low*m_numCovs ]; 
        m_Yl = new double[ m_low ];
    }

    incx = 1;
    m_up = 0;
    for (int i=0; i<m_numInds; i++ )
    {
        if ( m_Stemp[i] > evThreshold )
        {
            m_S[m_up] = m_Stemp[i];
            dcopy( &m_numInds, &m_Utemp[ i*m_numInds ], &incx, &m_U[m_up*m_numInds], &incx );
            m_up++;
        }
    }
    delete [] m_Utemp;
    delete [] m_Stemp;
}


void RoLMM::getProjection( double* P, double *X, int nrow, int ncol )
{
    double *Chol = new double[ ncol*ncol ];
    double *Solve = new double[ ncol*nrow ];

    transpose( X, Solve, nrow, ncol );

    double alpha=1.0, beta=0.0;
    dgemm( "T", "N", &ncol, &ncol, &nrow, &alpha, X, &nrow, X, &nrow, &beta, Chol, &ncol ); 
    
    // Compute the cholesky
    int info = 0;
    dpotrf( "U", &ncol, Chol, &ncol, &info );
    if ( info )
    {
        cerr << "getProjection: Cholesky Error: " << info << endl;
    }
    dpotrs( "U", &ncol, &nrow, Chol, &m_numCovs, Solve, &ncol, &info );
    if ( info )
    {
        cerr << "getProjection: Solve Error: " << info << endl;
    }
    
    dgemm( "N", "N", &nrow, &nrow, &ncol, &alpha, X, &nrow, Solve, &ncol, &beta, P, &nrow );

    delete [] Chol;
    delete [] Solve;

}


void RoLMM::setX( double *X, bool isLM )
{

    // Instead of copying use the stored matrices
    int incx = 1;
    int one = 1;

    if ( isLM )
    {
        int NM = m_numInds*m_numCovs;
        dcopy( &NM, X, &incx, m_X, &incx );
        return;
    }

    m_Xor = X;
    
    cout << "m_low: " << m_low << endl;
    cout << "m_up: " << m_up << endl;

    // Copy the rows m_k --> m_numInds to m_Xl;
    if ( m_low )
    {
        for (int i=0; i<m_numCovs; i++)
        {
            dcopy( &m_low, &m_Xor[ i*m_numInds + m_up ], &incx, &m_Xl[i*m_low], &incx );
        }
    }
   
    double alpha=1.0, beta=0.0;
    // rotate X
    dgemm( "T", "N", &m_up, &m_numCovs, &m_numInds, &alpha, m_U, &m_numInds, m_Xor, &m_numInds, &beta, m_Xu, &m_up );
    
    if ( m_low )
    {
        // Compute XltXl 
        dgemm( "T", "N", &m_numCovs, &m_numCovs, &m_low, &alpha, m_Xl, &m_low, m_Xl, &m_low, &beta, m_XltXl, &m_numCovs);
    }

    if ( m_low && m_Yl && !m_XltYl )
    {
        m_XltYl = new double[ m_numCovs ];
        dgemm( "T", "N", &m_numCovs, &one, &m_low, &alpha, m_Xl, &m_low, m_Yl, &m_low, &beta, m_XltYl, &m_numCovs );
    }


    // Get the eigendecomposition to compute the inverse and the determinant
    dgemm( "T", "N", &m_numCovs, &m_numCovs, &m_numInds, &alpha, m_Xor, &m_numInds, m_Xor, &m_numInds, &beta, m_XtX, &m_numCovs );

    double *eVecs = new double[ m_numCovs*m_numCovs ];
    double *eVals = new double[ m_numCovs ];
    
    getEigenDecomposition( m_XtX, m_numCovs, eVecs, eVals );
    // Determinant is the product of eigenvalues, invert eigenvalues for inverse
    m_detXtX = 1.0;
    for (int i=0; i<m_numCovs; i++) { m_detXtX *= eVals[i];  eVals[i] = 1.0/eVals[i];  }

    // Get the inverse;
    double *temp = new double[ m_numCovs*m_numCovs];
    double *temp2 = new double[ m_numInds*m_numCovs];

    int M = m_numCovs*m_numCovs;
    dcopy( &M, eVecs, &incx, temp, &incx );
    scaleColumns(  temp, eVals, m_numCovs, m_numCovs );
    dgemm( "N", "T", &m_numCovs, &m_numCovs, &m_numCovs, &alpha, temp, &m_numCovs, eVecs, &m_numCovs, &beta, m_invXtX, &m_numCovs);
    
    delete [] eVecs;
    delete [] eVals;
    delete [] temp;
    delete [] temp2;
}


void RoLMM::setYLMM( int phenNo )
{
    int one = 1;
   
    if ( m_low )
    {
        // Copy the low part
        dcopy( &m_low, &m_Yor[ phenNo*m_numInds + m_up], &one, m_Yl, &one );
    }

    double alpha=1.0, beta=0.0;
    dgemm( "T", "N", &m_up, &one, &m_numInds, &alpha, m_U, &m_numInds, &m_Yor[ phenNo*m_numInds], &m_numInds, &beta, m_Yu, &m_up );

    if ( m_low && m_Xl )
    {
        if ( !m_XltXl )
        {
            m_XltYl = new double[ m_numCovs ];
        }
        dgemm( "T", "N", &m_numCovs, &one, &m_low, &alpha, m_Xl, &m_low, m_Yl, &m_low, &beta, m_XltYl, &m_numCovs );
    }
}

void RoLMM::setY( double *Y, bool isLM )
{
    if ( isLM )
    {
        int incx = 1;
        int NM = m_numInds*m_numPhens;
        dcopy( &NM, Y, &incx, m_Y, &incx );
        return;
    }
    m_Yor = Y;
}


void RoLMM::transpose( double *source, double *target, int nrow, int ncol )
{
    int incx = 1;
    int incy = ncol;
    for ( int i=0; i<ncol; i++ )
    {
        dcopy( &nrow, &source[ i*nrow ], &incx, &target[ i ], &incy );
    }
}

void RoLMM::scaleColumns( double *mat, double *vec, int nrow, int ncol )
{
    int incx = 1;
    for ( int i=0; i < ncol; i++ )
    {
        dscal( &nrow, &vec[i], &mat[ i*nrow ], &incx );
    }
}

void RoLMM::scaleRows( double *mat, double *vec, int nrow, int ncol )
{
    int incx = nrow;
    for ( int i=0; i < nrow; i++ )
    {
        dscal( &ncol, &vec[i], &mat[ i ], &incx );
    }
}


void RoLMM::update()
{
    double beta=0.0, alpha=1.0;
    int one = 1;
    int incx = 1;
    
    // Beta = [ t(Xu) %*% D %*% Xu + t(Xl)Xl ]^-1 %*% [ t(Xu)%*%D%*%Yu + t(Xl)Yl ] 
    
    double *D = new double[ m_up ];
    for (int i=0; i<m_up; i++)
    {
        D[i] = 1.0 / ( exp(m_delta)*m_S[i] + 1.0 );
    }
    
    double *XutD = new double[ m_up*m_numCovs ];
    transpose( m_Xu, XutD, m_up, m_numCovs );
    scaleColumns( XutD, D, m_numCovs, m_up );
    
    int CC = m_numCovs*m_numCovs;
    
    double *XtDX = new double[ CC ];
    if ( m_low )
    {
        dcopy( &CC, m_XltXl, &incx, XtDX, &incx );
        dgemm( "N", "N", &m_numCovs, &m_numCovs, &m_up, &alpha, XutD, &m_numCovs, m_Xu, &m_up, &alpha, XtDX, &m_numCovs );
    }
    else
    {
        dgemm( "N", "N", &m_numCovs, &m_numCovs, &m_up, &alpha, XutD, &m_numCovs, m_Xu, &m_up, &beta, XtDX, &m_numCovs );
    }
    // XtDX =  t(Xu) %*% D %*% Xu + t(Xl)Xl 
    

    double *XtDY = new double[ m_numCovs ];
    if ( m_low )
    {
        dcopy( &m_numCovs, m_XltYl, &incx, XtDY, &incx );
        dgemm( "N", "N", &m_numCovs, &one, &m_up, &alpha, XutD, &m_numCovs, m_Yu, &m_up, &alpha, XtDY, &m_numCovs );
    }
    else
    {
        dgemm( "N", "N", &m_numCovs, &one, &m_up, &alpha, XutD, &m_numCovs, m_Yu, &m_up, &beta, XtDY, &m_numCovs );
    }
    // XDY =  t(Xu)%*%D%*%Yu + t(Xl)Yl 

    double *Chol = new double[ CC ];
    //if ( m_numCovs == 1 )
    //{
    //    // TRIVIAL
    //    m_beta[0] = XtDY[0] / XtDX[0]; 
    //}
    //else
    {
        // SOLVE FOR BETA USING CHOLESKY DECOMPOSITION

        dcopy( &CC, XtDX, &incx, Chol, &incx );
        dcopy( &m_numCovs, XtDY, &incx, m_beta, &incx );

        // Compute the cholesky
        int info = 0;
        dpotrf( "U", &m_numCovs, Chol, &m_numCovs, &info );
        if ( info != 0 )
        {
            cerr << "Beta: Cholesky Error: " << info << endl;
        }
        dpotrs( "U", &m_numCovs, &one, Chol, &m_numCovs, m_beta, &m_numCovs, &info );
        if ( info )
        {
            cerr << "Beta: Solve Error: " << info << endl;
        }
    }


    /*
     * BELOW COMPUTES THE DERIVATIVE OF BETA
     */

    // dBeta = -[ t(Xu) %*% D %*% Xu + t(Xl)Xl ]^-1 %*% [ t(Xu) %*% exp(d)*S%*%D^2] [ Yu - Xu %*% Beta ] 
    // VERY SIMILAR TO BETA COMPUTATION, WE ALREAY HAVE THE CHOL!!, JUST SOLVE FOR NEW RIGHT

    
    double *Xubeta = new double[ m_up ];
    double *YusXubeta = new double[ m_up ];
    double *XutSD2 = new double[ m_numCovs*m_up ];
    
    dgemm( "N", "N", &m_up, &one, &m_numCovs, &alpha, m_Xu, &m_up, m_beta, &m_numCovs, &beta, Xubeta, &m_up );
    // Xubeta
    for( int i=0; i<m_up; i++)
    {
        YusXubeta[i] = m_Yu[i] - Xubeta[i];
    }
    
    // RECYCLE XutD --> XutSD2
    double *eSD = new double[ m_up ];
    double *eSD2 = new double[ m_up ];
    
    int PC = m_up*m_numCovs;
    dcopy( &PC, XutD, &incx, XutSD2, &incx ); 
    
    for (int i=0; i<m_up; i++)
    {
        eSD[i] = exp(m_delta)*m_S[i] / ( exp(m_delta)*m_S[i] + 1.0 );
        eSD2[i] = exp(m_delta)*m_S[i] / pow( exp(m_delta)*m_S[i] + 1.0, 2 );
    }
    scaleColumns( XutSD2, eSD, m_numCovs, m_up );
    
    dgemm( "N", "N", &m_numCovs, &one, &m_up, &alpha, XutSD2, &m_numCovs, YusXubeta, &m_up, &beta, m_dbeta, &m_numCovs );

    //if ( m_numCovs == 1)
    //{
    //    // TRIVIAL
    //    m_dbeta[0] = m_dbeta[0] / XtDX[0]; 
    //}
    //else
    {
        // RECYCLE CHOL
        int info;
        dpotrs( "U", &m_numCovs, &one, Chol, &m_numCovs, m_dbeta, &m_numCovs, &info );
        if ( info )
        {
            cerr << "dBeta: Solve Error: " << info << endl;
        }
    }
    
    /*
     * HERE I HAVE BETA, dBETA and various cached stuff
     * NEXT: LL, REMLL
     */

    double *Xudbeta = new double[ m_up ];
    dgemm( "N", "N", &m_up, &one, &m_numCovs, &alpha, m_Xu, &m_up, m_dbeta, &m_numCovs, &beta, Xudbeta, &m_up );
    double *Xlbeta = NULL;
    double *Xldbeta = NULL;
    if ( m_low )
    {
        Xlbeta = new double[ m_low ];
        Xldbeta = new double[ m_low ];
        dgemm( "N", "N", &m_low, &one, &m_numCovs, &alpha, m_Xl, &m_low, m_beta, &m_numCovs, &beta, Xlbeta, &m_low );
        dgemm( "N", "N", &m_low, &one, &m_numCovs, &alpha, m_Xl, &m_low, m_dbeta, &m_numCovs, &beta, Xldbeta, &m_low );
    }
    
    // RSS = (Yu - XuBeta)^T %*% D %*% ( Yu - XuBeta ) + ( Yl - XlBeta)^T ( Yl - XlBeta)
    double RSS = 0.0;
    for ( int i=0; i<m_up; i++ )
    {
        RSS += pow( YusXubeta[i], 2 ) * D[i];
    }
    
    for ( int i=0; i<m_low; i++)
    {
        RSS += pow( m_Yl[i] - Xlbeta[i], 2 );
    }
    // RSS
    
    // dRSS = -2(Yu - XuBeta)^T %*% D %*% XudBeta ) -2( Yl - XlBeta)^T XldBeta) - (Yu - XuBeta)^T %*% eSD^2 %*% ( Yu - XuBeta ) 
    double dRSS = 0.0;
    for ( int i=0; i<m_up; i++ )
    {
        dRSS += -2.0*YusXubeta[i]*Xudbeta[i]*D[i];
    }
    for ( int i=0; i<m_low; i++ )
    {
        dRSS += -2.0*( m_Yl[i] - Xlbeta[i] )*Xldbeta[i];
    }
    for (int i=0; i<m_up; i++ )
    {
        dRSS += -pow( YusXubeta[i], 2 )*eSD2[i];
    }
    /*
     * GET LL AND REMLL
     */

    double n = (double)m_numInds;
    double p = (double)m_numCovs;
    double c = 1.837877066; // log(2pi)
    double t = 0.0;
    for (int i=0; i<m_up; i++)
    {
        t += log( exp(m_delta)*m_S[i] + 1.0 );
    }
    
    double logDet = 0.0;
    //if ( m_numCovs == 1 )
    //{
    //    
    //
    //}
    //else
    {
        for ( int i=0; i<m_numCovs; i++ )
        {
            logDet += log(  pow( Chol[ i*m_numCovs + i ], 2.0 ) );
        }
    }

    m_LL = -0.5*( n*(c+1.0) + t + n*log( RSS/n ) );
    m_REMLL = -0.5*( (n-p)*(c+1.0) + t - log( m_detXtX ) + logDet + (n-p)*log( RSS/(n-p) ) );


    /*********** dLL & dREMLL **************/
    double treSD = 0.0;
    for (int i=0; i<m_up; i++)
    {
        treSD += eSD[i];
    }

    m_dLL = -0.5*treSD 
            - 0.5*n*(dRSS/RSS);

    double trTerm = 0;
    // tr[  XtDX^-1 %*%  XutSD2Xu ]
    // I hace the CHOL for the the inverse term, I have XutSD2
    double *XutSD2Xu = new double[ m_numCovs*m_numCovs ];
    dgemm( "N", "N", &m_numCovs, &m_numCovs, &m_up, &alpha, XutSD2, &m_numCovs, m_Xu, &m_up, &beta, XutSD2Xu, &m_numCovs );
    int info;
    dpotrs( "U", &m_numCovs, &one, Chol, &m_numCovs, XutSD2Xu, &m_numCovs, &info );
    if ( info )
    {
        cerr << "dREMLL: Solve Error: " << info << endl;
    }
    trTerm = trace( XutSD2Xu, m_numCovs );


    m_dREMLL =  -0.5*treSD 
                + 0.5*trTerm 
                - 0.5*(n-p)*(dRSS/RSS);

    /*****************************************/
    
    m_sigmaE = RSS / n;
    m_sigmaEreml = RSS / (n-p);

    m_sigmaG = m_sigmaE*exp( m_delta );
    m_sigmaGreml = m_sigmaEreml*exp( m_delta );

    /*****BETA******/
    delete [] D;
    delete [] XutD;
    delete [] XtDX;
    delete [] XtDY;
    delete [] Chol;
    /*****************/

    /*****dBETA*******/
    delete [] Xubeta;
    delete [] YusXubeta;
    delete [] XutSD2;
    delete [] eSD;
    delete [] eSD2;
    /****************/

    /****RSS & dRSS****/
    delete [] Xudbeta;
    if ( Xlbeta ) delete [] Xlbeta;
    if ( Xldbeta ) delete [] Xldbeta;
    /*****************/

}



void RoLMM::getEigenDecomposition(double *X, int N, double *eVecs, double *eVals) // X is NxN
{

    int numEigenvalues, lda = N, ldz = N, il, iu, info;
    double abstol, vl, vu;
    
    double *work = NULL;
    int lwork;
    double wkopt;

    int *iwork = NULL, liwork, iwkopt;
    int *isuppz = new int[2*N];

    int N2 = N*N;
    int incx = 1;
    double *Xcpy = new double[ N2 ];
    dcopy(&N2, X, &incx, Xcpy, &incx );


   /* Query and allocate the optimal workspace */
    lwork = -1;
    liwork = -1;
    dsyevr( "V", // Jobz:   Compute Eigenvalues and EigenVectors 
            "A", // Range:  Compute all Eigenvalues
            "U", // Uplo:   Store the upper triangular part
            &N, Xcpy, &lda, 
            &vl, &vu, // Not referenced
            &il, &iu, // Not referenced
            &abstol, 
            &numEigenvalues, eVals, 
            eVecs, &ldz, 
            isuppz, 
            &wkopt, &lwork, &iwkopt, &liwork, &info );

    lwork = (int)wkopt;
    work = new double[lwork];
    liwork = iwkopt;
    iwork = new int[liwork];


    /* Solve eigenproblem */
    dsyevr( "V", // Jobz:   Compute Eigenvalues and EigenVectors 
            "A", // Range:  Compute all Eigenvalues
            "U", // Uplo:   Store the upper triangular part
            &N, Xcpy, &lda, 
            &vl, &vu, // Not referenced
            &il, &iu, // Not referenced
            &abstol, 
            &numEigenvalues, eVals, 
            eVecs, &ldz, 
            isuppz, 
            work, &lwork, iwork, &liwork, &info );
    if ( info > 0 )
    {
        cerr << "getEigenDecomposition: Error in eigedecomposition" << endl;
    }

    delete [] Xcpy;
    delete [] isuppz;
    delete [] work;
    delete [] iwork;
}



void RoLMM::findMax(bool isREML, bool isPlot)
{

    /*
    if ( isREML )
    {
        cout << "delta\tREMLL\tdREMLL" << endl;
    }
    else
    {
        cout << "delta\tLL\tdLL" << endl;
    }
    */

    if ( isPlot )
    {
        double delta = -20;
        while ( delta < 20 )
        {
            setDelta( delta );
            if ( isREML )
            {
                cerr << delta << "\t" << getREMLL() << "\t" << getdREMLL() << endl; //"\t" << getd2REMLL() << endl;
            }
            else
            {
                cerr << delta << "\t" << getLL() << "\t" << getdLL() << endl; //"\t" << getd2LL() << endl;
            }
            delta += 0.1;
        }
        return;
    }


    cout.precision( 12 );
    
    double delta;
    delta = -10.0;
    setDelta( delta );
/*
    if ( isREML )
    {
        cout << delta << "\t" << getREMLL() << "\t" << getdREMLL() << endl; //<< "\t" << getd2REMLL() << endl;
    }
    else
    {
        cout << delta << "\t" << getLL() << "\t" << getdLL() << endl; // << "\t" << getd2LL() << endl;
    }
*/
  
    
    double direction;
    double stepLength;
    double alpha = 0.01, beta = 0.5;
    double gt;
    double currentREMLL;
    double currentdREMLL;
    double deltaCheck;
    double newLLVal, newdLLVal;
    int num = -1;
    while( (num < 100 ) ||  ( isREML && fabs( getdREMLL() ) > 1e-5 ) || ( !isREML && fabs( getdLL() ) > 1e-5 )   )
    {
        num++;
        setDelta(delta);
        if ( isREML )
        {
            currentREMLL = getREMLL();
            currentdREMLL = getdREMLL();
        }
        else
        {
            currentREMLL = getLL();
            currentdREMLL = getdLL();
        }
 
        if ( isREML )
        {
            direction = getdREMLL() > 0.0 ? 1.0 : -1.0;//getd2REMLL();
        }
        else
        {
            direction = getdLL() > 0.0 ? 1.0 : -1.0;//getd2LL();
        }
        // HERE WE HAVE THE DIRECTION
        stepLength = 1.0/ ( num%10 + 1 );
        int rep;
        for ( rep=0; rep<500; rep++ )
        {
            
            gt = alpha*currentdREMLL*stepLength;
            
            deltaCheck = delta + stepLength*direction;
            setDelta( deltaCheck );
            
            newLLVal = isREML ? getREMLL() : getLL();
            newdLLVal = isREML ? getdREMLL() : getdLL();
            double cmpVal( currentREMLL + gt );
            
            if ( newLLVal > cmpVal )
            {
                delta = deltaCheck;
                break;
            }
            else
            {
                stepLength *= beta;
                newLLVal = currentREMLL;
                newdLLVal = currentdREMLL;
            }

        }
  /*      
        if ( isREML )
        {
            cout << delta << "\t" << getREMLL() << "\t" << getdREMLL() << endl; // "\t" << getd2REMLL() << endl;
        }
        else
        {
            cout << delta << "\t" << getLL() << "\t" << getdLL() << endl; //"\t" << getd2LL() << endl;
        }
  */    

        if (  ( newLLVal == currentREMLL ) && ( newdLLVal == currentdREMLL ) )
        {
            break;
        }
    }
    /*
    ofstream fp( m_infoFilename.c_str() );
    
    if ( isREML )
    {
        fp << "sigmaGreml: " << m_sigmaGreml << endl;
        fp << "sigmaEreml: " << m_sigmaEreml << endl;
    }
    else
    {
        fp << "sigmaG: " << m_sigmaG << endl;
        fp << "sigmaE: " << m_sigmaE << endl;

    }
    for ( int i=0; i < m_numCovs; i++ )
    {
        fp << "Beta[" << i << "]: " << m_beta[i] << endl; 
    }

    double h;
    if ( isREML )
    {
        h = getHeritability( m_sigmaGreml );
    }
    else
    {
        h = getHeritability( m_sigmaG );
    }
    cout << "h: " << h << endl;
    fp << "h: " << h << endl;
    
    fp.close();
    */
}


double RoLMM::getHeritability( double varE )
{
    int incx=1, one=1;
    double alpha=1.0, beta=0.0;
    double n = (double)m_numInds;

    double *PS = new double[ m_numInds*m_numInds ];
    double *PSP = new double[ m_numInds*m_numInds ];

    double *Py = new double[ m_numInds ];
     
    dgemm( "N", "N", &m_numInds, &m_numInds, &m_numInds, &alpha, m_P, &m_numInds, m_K, &m_numInds, &beta, PS, &m_numInds );

    dgemm( "N", "N", &m_numInds, &m_numInds, &m_numInds, &alpha, PS, &m_numInds, m_P, &m_numInds, &beta, PSP, &m_numInds );


    dgemm( "N", "N", &m_numInds, &one, &m_numInds, &alpha, m_P, &m_numInds, m_Yor, &m_numInds, &beta, Py, &m_numInds );
    double varY = ddot( &m_numInds, Py, &incx, m_Yor, &incx ) / (n-1.0);

    cout << "varY: " << varY << endl;
    
//    double trVal = 0.0;
//    for (int i=0; i<m_numInds; i++ )
//    {
//        trVal += varG*m_S[i];
//    }
    
//    cout << "tr: " << trVal << endl;
//    double h = trVal/varY;

    double varG = exp(m_delta)*varE;
    double trPSP = varG*trace( PSP, m_numInds ) / (n-1.0);
    cout << "trPSP: " << trPSP << endl;
    cout << "varY_exp: " << trPSP + varG*exp(m_delta) << endl;
    double h = trPSP / ( trPSP + varE );
    delete [] PS;
    delete [] PSP;
    delete [] Py;
    return h;

}


void RoLMM::testSNPsLM( bisectionSolver *rules )
{
    
    int N = m_numInds;
    int N2 = m_numInds*m_numInds;
    int incx = 1, one = 1;

    double alpha=1.0, beta=0.0;
  
    double *snpMatrix = m_reader->getSNPMatrix();
    double *Xalt = new double[ m_numInds*( m_numCovs + 1 ) ];
    double *XatXa = new double[ ( m_numCovs + 1 )*( m_numCovs + 1 ) ];
    double *XatY = new double [ ( m_numCovs + 1 ) ];
    double *Chol = new double[  ( m_numCovs + 1 ) *  ( m_numCovs + 1 )  ];

     
    double *Py = new double[ m_numInds ]; 
    
    ofstream fp;
    // F-test is the ratio of t(Y) ( M - M0 ) Y / r( M - M0 ) to t(Y)( I - M ) Y / r( I - M )
    // M is the projection matrix of the full model
    // M0 is the projection matrix of the Null model which is fixed so can be cached.
    // M needs to be recomputed for each SNP
    
    // X_null is X, X_alt is [ X_null snpVec ]

    int NM = m_numInds*m_numCovs;
    dcopy( &NM, m_X, &incx, Xalt, &incx );
    int M = ( m_numCovs + 1 ) ;
    int M2 = M*M;
 
    double *T = new double[ M ];
    double pvalue;
    m_dist = new boost::math::fisher_f( 1, (N-M) );
    int info;
    unsigned long numberofTests = 0;
    for ( int phenNo=0; phenNo<m_numPhens; phenNo++ )
    {
        
        double *pointY = &m_Y[ phenNo*m_numInds];

        // Quick sd computations of phenotype computation
        dgemm( "N", "N", &m_numInds, &one, &m_numInds, &alpha, m_P, &m_numInds, pointY , &m_numInds, &beta, Py, &m_numInds );
        double sdY = sqrt( ddot( &m_numInds, Py, &incx, pointY, &incx ) );

        dgemm( "T", "N", &m_numCovs, &one, &m_numInds, &alpha, Xalt, &m_numInds, pointY , &m_numInds, &beta, XatY, &M );

        // M0 stuff RSSM0 t(Y) M0 Y
        
        dcopy( &m_numCovs, XatY, &incx, T, &incx );

        int C2 = m_numCovs*m_numCovs;
        dgemm( "T", "N", &m_numCovs, &m_numCovs, &m_numInds, &alpha, Xalt, &m_numInds, Xalt, &m_numInds, &beta, XatXa, &m_numCovs );
        dcopy( &C2, XatXa, &incx, Chol, &incx );
        dpotrf( "U", &m_numCovs, Chol, &m_numCovs, &info );
        dpotrs( "U", &m_numCovs, &one, Chol, &m_numCovs, T, &m_numCovs, &info );
        double RSSM0( ddot( &m_numCovs, XatY, &incx, T, &incx ) );
        /***************************************/

        double YtY( ddot( &m_numInds, pointY, &incx, pointY, &incx ) );

        bool writeFlag = false;
        string filename(  m_pvalueFilename + "." + boost::lexical_cast<string>( phenNo ) );

        bisectionSolver::proxySNPRuleSet::const_iterator ruleIT;
        bisectionSolver::proxyNo_ID::const_iterator mapIT;
        
        for ( mapIT = rules->getProxyNo_ID()->begin(); mapIT != rules->getProxyNo_ID()->end(); mapIT++ )
        {
            unsigned int proxyNo( mapIT->first );
            unsigned int proxyID( mapIT->second );
            /*
             * TEST proxy
             */
            // add proxy vector
            unsigned int snp = proxyID;
            dcopy( &m_numInds, &snpMatrix[ snp*m_numInds ], &incx, &Xalt[ m_numInds*m_numCovs ], &incx );
            XatY[ m_numCovs ] = ddot( &m_numInds,  &snpMatrix[ snp*m_numInds ], &incx, pointY, &incx );
            dcopy( &M, XatY, &incx, T, &incx );
            dgemm( "T", "N", &M, &M, &m_numInds, &alpha, Xalt, &m_numInds, Xalt, &m_numInds, &beta, XatXa, &M );
            dcopy( &M2, XatXa, &incx, Chol, &incx );
            dpotrf( "U", &M, Chol, &M, &info );
            dpotrs( "U", &M, &one, Chol, &M, T, &M, &info ); // here T is the beta vector
            double snpBeta( T[m_numCovs] );
            double RSSM( ddot( &M, XatY, &incx, T, &incx ) );
           
            double stat( (RSSM - RSSM0)*( N - M ) / (  (YtY - RSSM ) )  );       
            numberofTests++;
            if ( !isnan( stat ) )
            {
                if ( stat < 0.0 )
                {
                    pvalue = 1.0;
                    stat = 0.0;
                }
                else
                {
                    pvalue = boost::math::cdf( boost::math::complement( *m_dist, stat) );
                }
                //double pvalue( boost::math::cdf( *m_dist, stat) );
                if ( pvalue < m_pfilter )
                {
                    if ( !writeFlag )
                    {
                        fp.open( filename.c_str() );
                        fp << "SNP\tCHR\tBP\tP\tSTAT\n";
                        writeFlag = true;
                    }

                    fp  << m_reader->getSNPrsid( snp ) << "\t"
                        << m_reader->getSNPchr( snp ) << "\t"
                        << m_reader->getSNPpos( snp ) << "\t"
                        << pvalue << "\t"
                        << sqrt(stat) << endl;
                }
            }


            double proxyStat( fabs( stat ) );
            for ( ruleIT = rules->getProxyRules()->at( proxyNo ).begin(); ruleIT != rules->getProxyRules()->at( proxyNo ).end(); ruleIT++ )
            {
                if ( ruleIT->statThreshold > proxyStat )
                {
                    break;
                }
                
                unsigned int snp = ruleIT->remainderId;
                dcopy( &m_numInds, &snpMatrix[ snp*m_numInds ], &incx, &Xalt[ m_numInds*m_numCovs ], &incx );
                XatY[ m_numCovs ] = ddot( &m_numInds,  &snpMatrix[ snp*m_numInds ], &incx,  pointY , &incx );
                dcopy( &M, XatY, &incx, T, &incx );
                
                dgemm( "T", "N", &M, &M, &m_numInds, &alpha, Xalt, &m_numInds, Xalt, &m_numInds, &beta, XatXa, &M );
                dcopy( &M2, XatXa, &incx, Chol, &incx );
                dpotrf( "U", &M, Chol, &M, &info );
                dpotrs( "U", &M, &one, Chol, &M, T, &M, &info ); // here T is the beta vector
                double snpBeta( T[m_numCovs] );

                double RSSM( ddot( &M, XatY, &incx, T, &incx ) );
               
                double stat( (RSSM - RSSM0)*( N - M ) / (  (YtY - RSSM ) )  );       
                numberofTests++;
                if ( !isnan( stat ) )
                {
                    if ( stat < 0.0 )
                    {
                        pvalue = 1.0;
                        stat = 0.0;
                    }
                    else
                    {
                        pvalue = boost::math::cdf( boost::math::complement( *m_dist, stat) );
                    }
                    //double pvalue( boost::math::cdf( *m_dist, stat) );
                    if ( pvalue < m_pfilter )
                    {
                        if ( !writeFlag )
                        {
                            fp.open( filename.c_str() );
                            fp << "SNP\tCHR\tBP\tP\tSTAT\n";
                            writeFlag = true;
                        }

                        fp  << m_reader->getSNPrsid( snp ) << "\t"
                            << m_reader->getSNPchr( snp ) << "\t"
                            << m_reader->getSNPpos( snp ) << "\t"
                            << pvalue << "\t"
                            << sqrt(stat) << endl;
                    }
                }
            }
        }
        if ( writeFlag )
        {
            fp.close();
        }
    }
    string filename(  m_pvalueFilename + ".info" );
    fp.open( filename.c_str() );
    fp << 1.0 - (double)numberofTests / (double)( m_numPhens*m_numSNPs ) << endl;
    fp << "#testPerformed: " << numberofTests << endl;
    fp << "#allTests: " << m_numPhens*m_numSNPs << endl;
    fp.close();

    delete [] T;
    delete [] Py;
    delete [] Xalt;
    delete [] XatXa;
    delete [] XatY;
    delete [] Chol;
}




void RoLMM::testSNPsLMM( bisectionSolver *rules, bool isREML )
{

    int N = m_numInds;
    int N2 = m_numInds*m_numInds;
    int incx = 1, one = 1;

    double alpha=1.0, beta=0.0;
    
    // get the inverse sqrt of K
    double *invKsqrt = new double[ N2 ];
    double *eVecs = new double[ N2 ];
    double *eVecs2 = new double[ N2 ];
    double *eVals = new double[ m_numInds ];
    double *snpMatrix = new double[ m_numInds*m_numSNPs];
    

    int M = ( m_numCovs + 1 ) ;
    int M2 = M*M;
    int info;

    double *T = new double[ M ];
    m_dist = new boost::math::fisher_f( 1, (N-M) );
    double *Py = new double[ m_numInds ];

    double *Xalt = new double[ m_numInds*( m_numCovs + 1 ) ];
    double *XatXa = new double[ ( m_numCovs + 1 )*( m_numCovs + 1 ) ];
    double *XatY = new double [ ( m_numCovs + 1 ) ];
    double *Chol = new double[  ( m_numCovs + 1 ) *  ( m_numCovs + 1 )  ];

    double *Kcpy = new double[ N2 ];
    
    // Here I have K^(-1/2)
    // Now rotate everything...
    unsigned long numberofTests = 0;
    for ( int phenNo = 0; phenNo < m_numPhens; phenNo++ )
    {
        
        // We do F-test here
        // Upto here the correct kinship is fitted using REML
        // Get the K^(-1/2) and rotate phenotype , SNPs and covariates
        // Use OLS to test hypothesis each SNP effect is == 0?
       
        this->setYLMM( phenNo );
        this->findMax( isREML, false ); 
        
        double *pointY = &m_Y[ phenNo*m_numInds]; 

        double varG = isREML ? m_sigmaGreml : m_sigmaG;
        double varE = isREML ? m_sigmaEreml : m_sigmaE;
      

        dcopy( &N2, m_K, &incx, Kcpy, &incx );
        dscal( &N2, &varG, Kcpy, &incx );
        for ( int i = 0; i<m_numInds; i++ )
        {
            Kcpy[ i*m_numInds + i ] += varE;
        }
        
        /*
        
        int UN = m_up*m_numInds;
        double *US = new double[ UN ];
        dcopy( &UN, m_U, &incx, US, &incx );
        scaleColumns( US, m_S, m_numInds, m_up );
        double scale = exp(m_delta);
        dgemm( "N", "T", &m_numInds, &m_numInds, &m_up, &scale, US, &m_numInds, m_U, &m_numInds, &beta, m_K, &m_numInds);
        delete [] US;
        for ( int i=0; i<m_numInds; i++)
        {
            m_K[ i*m_numInds + i ] += 1.0;
        }
        dscal( &N2, &varE, m_K, &incx );
        */


        

        getEigenDecomposition( Kcpy, m_numInds, eVecs, eVals );
        vdInvSqrt( m_numInds, eVals, eVals );
        dcopy( &N2, eVecs, &incx, eVecs2, &incx );
        scaleColumns( eVecs2, eVals, m_numInds, m_numInds );
        dgemm( "N", "T", &m_numInds, &m_numInds, &m_numInds, &alpha, eVecs2, &m_numInds, eVecs, &m_numInds, &beta, invKsqrt, &m_numInds );

        dgemm( "N", "N", &m_numInds, &one, &m_numInds, &alpha, invKsqrt, &m_numInds, &m_Yor[ phenNo*m_numInds ], &m_numInds, &beta, pointY, &m_numInds );
        dgemm( "N", "N", &m_numInds, &m_numSNPs, &m_numInds, &alpha, invKsqrt, &m_numInds, m_snpMatrix, &m_numInds, &beta, snpMatrix, &m_numInds );
        dgemm( "N", "N", &m_numInds, &m_numCovs, &m_numInds, &alpha, invKsqrt, &m_numInds, m_Xor, &m_numInds, &beta, m_X, &m_numInds );

        // Quick sd computations of phenotype computation
        dgemm( "N", "N", &m_numInds, &one, &m_numInds, &alpha, m_P, &m_numInds, pointY, &m_numInds, &beta, Py, &m_numInds );
        double sdY = sqrt( ddot( &m_numInds, Py, &incx, pointY, &incx ) );

        // F-test is the ratio of t(Y) ( M - M0 ) Y / r( M - M0 ) to t(Y)( I - M ) Y / r( I - M )
        // M is the projection matrix of the full model
        // M0 is the projection matrix of the Null model which is fixed so can be cached.
        // M needs to be recomputed for each SNP
        
        // X_null is X, X_alt is [ X_null snpVec ]
        
        int NM = m_numInds*m_numCovs;
        dcopy( &NM, m_X, &incx, Xalt, &incx );

        dgemm( "T", "N", &m_numCovs, &one, &m_numInds, &alpha, Xalt, &m_numInds, pointY, &m_numInds, &beta, XatY, &M );
                
        // M0 stuff RSSM0 t(Y) M0 Y
        dcopy( &m_numCovs, XatY, &incx, T, &incx );

        int C2 = m_numCovs*m_numCovs;
        dgemm( "T", "N", &m_numCovs, &m_numCovs, &m_numInds, &alpha, Xalt, &m_numInds, Xalt, &m_numInds, &beta, XatXa, &m_numCovs );
        dcopy( &C2, XatXa, &incx, Chol, &incx );
        dpotrf( "U", &m_numCovs, Chol, &m_numCovs, &info );
        dpotrs( "U", &m_numCovs, &one, Chol, &m_numCovs, T, &m_numCovs, &info );
        double RSSM0( ddot( &m_numCovs, XatY, &incx, T, &incx ) );
        /***************************************/

        double YtY( ddot( &m_numInds, pointY, &incx, pointY, &incx ) );
        

        double pvalue;

        bool writeFlag = false;
        string filename(  m_pvalueFilename + "." + boost::lexical_cast<string>( phenNo ) );
        ofstream fp;

        bisectionSolver::proxySNPRuleSet::const_iterator ruleIT;
        bisectionSolver::proxyNo_ID::const_iterator mapIT;

        for ( mapIT = rules->getProxyNo_ID()->begin(); mapIT != rules->getProxyNo_ID()->end(); mapIT++ )
        {
            unsigned int proxyNo( mapIT->first );
            unsigned int proxyID( mapIT->second );

            /*
             * TEST proxy
             */
            // add proxy vector
            unsigned int snp = proxyID;
            dcopy( &m_numInds, &snpMatrix[ snp*m_numInds ], &incx, &Xalt[ m_numInds*m_numCovs ], &incx );
            XatY[ m_numCovs ] = ddot( &m_numInds,  &snpMatrix[ snp*m_numInds ], &incx, pointY, &incx );
            dcopy( &M, XatY, &incx, T, &incx );
            
            dgemm( "T", "N", &M, &M, &m_numInds, &alpha, Xalt, &m_numInds, Xalt, &m_numInds, &beta, XatXa, &M );
            dcopy( &M2, XatXa, &incx, Chol, &incx );
            dpotrf( "U", &M, Chol, &M, &info );
            dpotrs( "U", &M, &one, Chol, &M, T, &M, &info ); // here T is the beta vector
            double snpBeta( T[m_numCovs] );

            double RSSM( ddot( &M, XatY, &incx, T, &incx ) );
           
            double stat( (RSSM - RSSM0)*( N - M ) / (  (YtY - RSSM ) )  );       
            numberofTests++;
            if ( !isnan( stat ) )
            {
                if ( stat < 0.0 )
                {
                    pvalue = 1.0;
                    stat = 0.0;
                }
                else
                {
                    pvalue = boost::math::cdf( boost::math::complement( *m_dist, stat) );
                }
                //double pvalue( boost::math::cdf( *m_dist, stat) );
                if ( pvalue < m_pfilter )
                {
                    if ( !writeFlag )
                    {
                        fp.open( filename.c_str() );
                        fp << "SNP\tCHR\tBP\tP\tSTAT\n";
                        writeFlag = true;
                    }

                    fp  << m_reader->getSNPrsid( snp ) << "\t"
                        << m_reader->getSNPchr( snp ) << "\t"
                        << m_reader->getSNPpos( snp ) << "\t"
                        << pvalue << "\t"
                        << sqrt(stat) << endl;
                }
            }

            double proxyStat( fabs( stat ) );
            for ( ruleIT = rules->getProxyRules()->at( proxyNo ).begin(); ruleIT != rules->getProxyRules()->at( proxyNo ).end(); ruleIT++ )
            {
                if ( ruleIT->statThreshold > proxyStat )
                {
                    break;
                }
                
                unsigned int snp = ruleIT->remainderId;
                dcopy( &m_numInds, &snpMatrix[ snp*m_numInds ], &incx, &Xalt[ m_numInds*m_numCovs ], &incx );
                XatY[ m_numCovs ] = ddot( &m_numInds,  &snpMatrix[ snp*m_numInds ], &incx, pointY, &incx );
                dcopy( &M, XatY, &incx, T, &incx );
                
                dgemm( "T", "N", &M, &M, &m_numInds, &alpha, Xalt, &m_numInds, Xalt, &m_numInds, &beta, XatXa, &M );
                dcopy( &M2, XatXa, &incx, Chol, &incx );
                dpotrf( "U", &M, Chol, &M, &info );
                dpotrs( "U", &M, &one, Chol, &M, T, &M, &info ); // here T is the beta vector
                double snpBeta( T[m_numCovs] );

                double RSSM( ddot( &M, XatY, &incx, T, &incx ) );
               
                double stat( (RSSM - RSSM0)*( N - M ) / (  (YtY - RSSM ) )  );       
                numberofTests++;
                if ( !isnan( stat ) )
                {
                    if ( stat < 0.0 )
                    {
                        pvalue = 1.0;
                        stat = 0.0;
                    }
                    else
                    {
                        pvalue = boost::math::cdf( boost::math::complement( *m_dist, stat) );
                    }
                    //double pvalue( boost::math::cdf( *m_dist, stat) );
                    if ( pvalue < m_pfilter )
                    {
                        if ( !writeFlag )
                        {
                            fp.open( filename.c_str() );
                            fp << "SNP\tCHR\tBP\tP\tSTAT\n";
                            writeFlag = true;
                        }

                        fp  << m_reader->getSNPrsid( snp ) << "\t"
                            << m_reader->getSNPchr( snp ) << "\t"
                            << m_reader->getSNPpos( snp ) << "\t"
                            << pvalue << "\t"
                            << sqrt(stat) << endl;
                    }
                }
            }
        }
        if ( writeFlag )
        {
            fp.close();
        }
    }
    ofstream fp;
    string filename( m_pvalueFilename + ".info" ); 
    fp.open( filename.c_str() );
    fp << 1.0 - (double)numberofTests / (double)( m_numPhens*m_numSNPs ) << endl;
    fp << "#testPerformed: " << numberofTests << endl;
    fp << "#allTests: " << m_numPhens*m_numSNPs << endl;
    fp.close();
    delete [] Kcpy;
    delete [] Py;
    delete [] T;
    delete [] Xalt;
    delete [] XatXa;
    delete [] XatY;
    delete [] Chol;
    delete [] invKsqrt;
    delete [] eVecs;
    delete [] eVecs2;
    delete [] eVals;
}




void RoLMM::testSNPs( bool isREML )
{
        
    
    // We do F-test here
    // Upto here the correct kinship is fitted using REML
    // Get the K^(-1/2) and rotate phenotype , SNPs and covariates
    // Use OLS to test hypothesis each SNP effect is == 0?
    
    double varG = isREML ? m_sigmaGreml : m_sigmaG;
    double varE = isREML ? m_sigmaEreml : m_sigmaE;

    int N = m_numInds;
    int N2 = m_numInds*m_numInds;
    int incx = 1, one = 1;

    /*
    // this is the true kinship
    dscal( &N2, &varG, m_K, &incx );
    for ( int i = 0; i<m_numInds; i++ )
    {
        m_K[ i*m_numInds + i ] += varE;
    }
    */

    double alpha=1.0, beta=0.0;
    
    int UN = m_up*m_numInds;
    double *US = new double[ UN ];
    dcopy( &UN, m_U, &incx, US, &incx );
    scaleColumns( US, m_S, m_numInds, m_up );
    double scale = exp(m_delta);
    dgemm( "N", "T", &m_numInds, &m_numInds, &m_up, &scale, US, &m_numInds, m_U, &m_numInds, &beta, m_K, &m_numInds);
    delete [] US;
    for ( int i=0; i<m_numInds; i++)
    {
        m_K[ i*m_numInds + i ] += 1.0;
    }
    dscal( &N2, &varE, m_K, &incx );


    // get the inverse sqrt of K
    double *invKsqrt = new double[ N2 ];
    double *eVecs = new double[ N2 ];
    double *eVecs2 = new double[ N2 ];
    double *eVals = new double[ m_numInds ];
    double *snpMatrix = new double[ m_numInds*m_numSNPs];
    
    getEigenDecomposition( m_K, m_numInds, eVecs, eVals );
    vdInvSqrt( m_numInds, eVals, eVals );
    dcopy( &N2, eVecs, &incx, eVecs2, &incx );
    scaleColumns( eVecs2, eVals, m_numInds, m_numInds );
    dgemm( "N", "T", &m_numInds, &m_numInds, &m_numInds, &alpha, eVecs2, &m_numInds, eVecs, &m_numInds, &beta, invKsqrt, &m_numInds );

    // Here I have K^(-1/2)
    // Now rotate everything...

    dgemm( "N", "N", &m_numInds, &one, &m_numInds, &alpha, invKsqrt, &m_numInds, m_Yor, &m_numInds, &beta, m_Y, &m_numInds );
    dgemm( "N", "N", &m_numInds, &m_numSNPs, &m_numInds, &alpha, invKsqrt, &m_numInds, m_snpMatrix, &m_numInds, &beta, snpMatrix, &m_numInds );
    dgemm( "N", "N", &m_numInds, &m_numCovs, &m_numInds, &alpha, invKsqrt, &m_numInds, m_Xor, &m_numInds, &beta, m_X, &m_numInds );
    
    // Everything is rotated...
    
    // Quick sd computations of phenotype computation
    double *Py = new double[ m_numInds ];
    dgemm( "N", "N", &m_numInds, &one, &m_numInds, &alpha, m_P, &m_numInds, m_Y, &m_numInds, &beta, Py, &m_numInds );
    double sdY = sqrt( ddot( &m_numInds, Py, &incx, m_Y, &incx ) );
    delete [] Py;

    
    // F-test is the ratio of t(Y) ( M - M0 ) Y / r( M - M0 ) to t(Y)( I - M ) Y / r( I - M )
    // M is the projection matrix of the full model
    // M0 is the projection matrix of the Null model which is fixed so can be cached.
    // M needs to be recomputed for each SNP
    
    // X_null is X, X_alt is [ X_null snpVec ]

    double *Xalt = new double[ m_numInds*( m_numCovs + 1 ) ];
    double *XatXa = new double[ ( m_numCovs + 1 )*( m_numCovs + 1 ) ];
    double *XatY = new double [ ( m_numCovs + 1 ) ];
    double *Chol = new double[  ( m_numCovs + 1 ) *  ( m_numCovs + 1 )  ];
    int NM = m_numInds*m_numCovs;
    dcopy( &NM, m_X, &incx, Xalt, &incx );

    int M = ( m_numCovs + 1 ) ;
    int M2 = M*M;
    dgemm( "T", "N", &m_numCovs, &one, &m_numInds, &alpha, Xalt, &m_numInds, m_Y, &m_numInds, &beta, XatY, &M );
    int info;
    
    double *T = new double[ M ];

    // M0 stuff RSSM0 t(Y) M0 Y
    
    dcopy( &m_numCovs, XatY, &incx, T, &incx );

    int C2 = m_numCovs*m_numCovs;
    dgemm( "T", "N", &m_numCovs, &m_numCovs, &m_numInds, &alpha, Xalt, &m_numInds, Xalt, &m_numInds, &beta, XatXa, &m_numCovs );
    dcopy( &C2, XatXa, &incx, Chol, &incx );
    dpotrf( "U", &m_numCovs, Chol, &m_numCovs, &info );
    dpotrs( "U", &m_numCovs, &one, Chol, &m_numCovs, T, &m_numCovs, &info );
    double RSSM0( ddot( &m_numCovs, XatY, &incx, T, &incx ) );
    /***************************************/

    double YtY( ddot( &m_numInds, m_Y, &incx, m_Y, &incx ) );

    double pvalue;

    m_dist = new boost::math::fisher_f( 1, (N-M) );
    bool writeFlag = false;
    ofstream fp;
    for ( int snp = 0; snp < m_numSNPs; snp++ )
    {
        
        //get the sd of the SNP
        double sdSNP( sqrt( 2.0*m_reader->getSNPmaf( snp )*( 1.0 - m_reader->getSNPmaf(snp) ) ) );
        
        // add snp vector;
        dcopy( &m_numInds, &snpMatrix[ snp*m_numInds ], &incx, &Xalt[ m_numInds*m_numCovs ], &incx );
        XatY[ m_numCovs ] = ddot( &m_numInds,  &snpMatrix[ snp*m_numInds ], &incx, m_Y, &incx );
        dcopy( &M, XatY, &incx, T, &incx );
        
        dgemm( "T", "N", &M, &M, &m_numInds, &alpha, Xalt, &m_numInds, Xalt, &m_numInds, &beta, XatXa, &M );
        dcopy( &M2, XatXa, &incx, Chol, &incx );
        dpotrf( "U", &M, Chol, &M, &info );
        dpotrs( "U", &M, &one, Chol, &M, T, &M, &info ); // here T is the beta vector
        double snpBeta( T[m_numCovs] );

        double RSSM( ddot( &M, XatY, &incx, T, &incx ) );
       
        double stat( (RSSM - RSSM0)*( N - M ) / (  (YtY - RSSM ) )  );       

        if ( !isnan( stat ) )
        {
            if ( stat < 0.0 )
            {
                pvalue = 1.0;
                stat = 0.0;
            }
            else
            {
                pvalue = boost::math::cdf( boost::math::complement( *m_dist, stat) );
            }
            //double pvalue( boost::math::cdf( *m_dist, stat) );
            if ( pvalue < m_pfilter )
            {
                if ( !writeFlag )
                {
                    fp.open( m_pvalueFilename.c_str() );
                    fp << "SNP\tCHR\tBP\tP\tSTAT\tBETA\tMAF\n";
                    writeFlag = true;
                }

                fp  << m_reader->getSNPrsid( snp ) << "\t"
                    << m_reader->getSNPchr( snp ) << "\t"
                    << m_reader->getSNPpos( snp ) << "\t"
                    << pvalue << "\t"
                    << sqrt(stat) << "\t"
                    << snpBeta*sdSNP/sdY << "\t"
                    << m_reader->getSNPmaf( snp ) << endl;
            }
        }
    }
    if ( writeFlag )
    {
        fp.close();
    }


    delete [] Xalt;
    delete [] XatXa;
    delete [] XatY;
    delete [] Chol;
    delete [] invKsqrt;
    delete [] eVecs;
    delete [] eVecs2;
    delete [] eVals;

}




