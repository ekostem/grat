/*
 * =====================================================================================
 *
 *       Filename:  mathHelper.cpp
 *
 *    Description:  mathHelper implementations
 *
 *        Version:  1.0
 *        Created:  10/19/2011 01:49:22
 *       Revision:  none
 *       Compiler:  icpc
 *
 *         Author:  EMRAH KOSTEM (ekostem), 
 *        Company:  www.emrahkostem.com 
 *
 * =====================================================================================
 */

#include "../include/mathHelper.h"

string itoa(int value, int base) 
{
            
    string buf;
                    
    // check that the base if valid
    if (base < 2 || base > 16) return buf;
            
    enum { kMaxDigits = 35 };
    buf.reserve( kMaxDigits ); // Pre-allocate enough space.
                                         
                                            
    int quotient = value;
                                                            
    // Translating number to string with base:
    do 
    {
        buf += "0123456789abcdef"[ abs( quotient % base ) ];
        quotient /= base;
    } while ( quotient );
                                                
    // Append the negative sign
    if ( value < 0) buf += '-';
                                                                                                                                        
    reverse( buf.begin(), buf.end() );
    return buf;
}

int pnormVec(const double* x, double* output, const int n)
{
    if ( !x || !output )
    {
        return 0;
    }
    vdCdfNorm(n, x, output);
    return 1;
}

int dnormVec(const double* x, double* output, const int n)
{
    if (!x || !output )
    {
        return 0;
    }
    double *scale = new double[n];
    for (int i=0; i<n; i++)
    {
        scale[i] = M_SQRT2PI;      
    }

    vdSqr(n, x, output);
    for (int i=0; i<n; i++)
    {
        output[i] *= -0.5;
    }
    vdExp(n, output, output);
    vdMul(n, scale, output, output);

    delete [] scale;
    return 1;

}


int qnormVec(const double* x, const double* mean, const double* var, double* output, const int n)
{
    if ( !x || !mean || !var || !output )
    {
        return 0;
    }
    double *std = new double[n];
    vdSqrt(n, var, std);
    vdCdfNormInv(n, x, output);
    vdMul(n, std, output, output);
    vdAdd(n, mean, output, output);
    delete [] std;
    return 1;
}


double computeQuadrantProb(double s, double t, double mean1, double mean2, double rho)
{
    return  ND2( -s-mean1, -t-mean2, rho ) + 
            nc( -t-mean2 ) - ND2( s-mean1, -t-mean2, rho) + 
            1.0 - nc( t-mean2) - nc( s-mean1) + ND2( s-mean1, t-mean2, rho) +
            nc( -s-mean1 ) - ND2( -s-mean1, t-mean2, rho );
}


double computeQuadrantProb(RandomVariable *rv, double s, double t, double mean1, double mean2, double rho, int sampleSize)
{
    if ( !rv || sampleSize < 1 || fabs(rho) > 1 )
    {
        return 0.0;
    }
    double *sigma = new double[4];  
    double *mean = new double[2];
    double *samples = new double[2*sampleSize];
    double prob = 0.0;
    sigma[0] = 1.0;
    sigma[1] = rho;
    sigma[2] = rho;
    sigma[3] = 1.0;

    mean[0] = mean1;
    mean[1] = mean2;

    rv->mvgenerate_normal(sampleSize, 2, mean, sigma, samples);
    for (int i=0; i<sampleSize; i++)
    {
        if ( (samples[2*i] < -s) && (samples[2*i+1] < -t) )
        {
            prob += 1.0;
        }
        else if ( (samples[2*i] < -s) && (samples[2*i+1] > t) )
        {
            prob += 1.0;
        }
        else if ( (samples[2*i] > s) && (samples[2*i+1] < -t) )
        {
            prob += 1.0;
        }
        else if ( (samples[2*i] > s) && (samples[2*i+1] > t) )
        {
            prob += 1.0;
        }
    }

    delete [] sigma;
    delete [] mean;
    delete [] samples;
    return prob/(double)sampleSize;
}


RandomVariable::RandomVariable(const int seed, const int brng)
{
    brng_ = brng;
    seed_ = seed;
    vslNewStream( &stream_, brng_, seed_ );
}

void RandomVariable::generate_normal(const int number_variables, const int mean, const int std, double* output, const int generate_method)
{
    vdRngGaussian( generate_method, stream_, number_variables, output, mean, std );
}

void RandomVariable::mvgenerate_normal(const int number_variables, int dimension, const double* mean, double* variance_covariance_matrix, double* output, const int generate_method)
{
    int info;
    dpotrf("U", &dimension, variance_covariance_matrix, &dimension , &info);
    vdRngGaussianMV( generate_method, stream_, number_variables, output, dimension, VSL_MATRIX_STORAGE_FULL, mean, variance_covariance_matrix );
}

void RandomVariable::generate_uniform(const int number_variables, const double left, const double right, double* output, const int generate_method)
{
    vdRngUniform( generate_method, stream_, number_variables, output, left, right );
}





